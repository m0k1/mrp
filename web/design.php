<!DOCTYPE html>
<html lang="en">
<head>
	<title>mObjectAdder</title>
	<meta charset="utf-8" />
	<meta name="description" content="Add object to MRP GM" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
	<script src="js/textAreaResize.js"></script>
	<script src="js/fancyZoom.js"></script>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
</head>
<body>
	<div id="formatOverlay"></div>
	<div id="changeDrawDistance" class="jsHide">
		<strong class="largeText">Change object stream distance</strong><br /><br />
		<input id="drawDistance" type="text" value="200" maxlength="14" size="15"/><br /><br />
		<a href="#" onClick="$('#optionsDrawDistance').children('p').text('No change');$('#zoomClose').click();return false" class="blackText">
		Click here to keep the input value<br />(This will default to 200 if there is no current value found)</a><br />
	</div>
	<div id="changeStreamDistance" class="jsHide">
		<strong class="largeText">Change object stream distance</strong><br /><br />
		<input id="streamDistance" type="text" value="200" maxlength="14" size="15"/><br /><br />
		<a href="#" onClick="$('#optionsStreamDistance').children('p').text('No change');$('#zoomClose').click();return false" class="blackText">
		Click here to keep the input value<br />(This will default to 200 if there is no current value found)</a><br />
	</div>
	<div id="zoomOverlay"></div>
	<div class="main-header">
	
		<a href="/"><div class="header-logo">MRP GM</div></a>
		<div class="header-services">
			<ul>

				<a href="/addremove.php"><li><i class="fa fa-building-o"></i>Remove Adder</li></a>
				<a href="/addmaterial.php"><li><i class="fa fa-bullseye"></i>Material Adder</li></a>
				<a href="/addobject.php"><li><i class="fa fa-plus"></i>Object Adder</li></a>
			</ul>
		</div>
		
	</div>
	<!-- Main page !-->
	<div id="wrapper">
		<div id="pageContainer">
							<div id="inputArea">
					<div class="resizable relative">
						<div class="paste-text">Paste Objects Here</div>
						<form>
						<textarea id="inputAreaElement" wrap="off"></textarea>
					</div>
				</div>
				<div id="optionsBlock" class="spriteBg">
					<div id="optionsSprite" class="sprite"></div>
					<div id="optionsBlockLeft">
						<a href="#changeDrawDistance" id="optionsDrawDistance">Draw-distance <p>200</p></a><br />
					</div>
					<div id="optionsBlockRight">
						<a href="#changeStreamDistance" id="optionsStreamDistance">Stream-distance <p>200</p></a><br />
					</div>
					<div style="text-align: center; margin-top: 70px; color: #FFF;">Ukupno <a style='color: #FFF;'>1337</a> objekata i <a style='color: #FFF;'>1337</a> textura je u bazi.</div>
				</div>
				<div id="convertButton" style="margin: 0px;">
					<div class='action-button' style="margin: 0px;">Add Object's</div>
				</div>
				</form>
			</div>
	</div>
	<div id="zoom" class="jsHide">
		<div id="zoomContent"></div>
		<div id="zoomClose" class="spriteBg">
			<div class="sprite close"></div>
		</div>
	</div>
</body>
<script>
$("textAreaResize").css("margin-right","0px");
$( '.jsHide' ) . css( { 'display' : 'none' } );
$('#optionsDrawDistance').fancyZoom({width: '350', height: '182'});
$('#optionsStreamDistance').fancyZoom({width: '350', height: '182'});
$('textarea').textAreaResizer();
if(document.documentElement.clientHeight < 561)
{
	$('#wrapper').css({margin : '0em auto 0px'});
}
$("#drawDistance").bind("change paste keyup", function() {
   alert($(this).val()); 
});
</script>
</html>
