#include <a_samp>
#undef MAX_PLAYERS
#define MAX_PLAYERS	(1000)
#include <core>
#include <float>
#include <YSI\y_commands>											// By Y_Less, 4.0:					https://github.com/Misiur/YSI-Includes/
#include <YSI\y_master>													// By Y_Less, 4.0:					https://github.com/Misiur/YSI-Includes/

main()
{
	print("\n----------------------------------");
	print("  Bare Script\n");
	print("----------------------------------\n");
}

public OnPlayerSpawn(playerid)
{
	SetPlayerInterior(playerid,0);
	TogglePlayerClock(playerid,0);
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	SetPlayerInterior(playerid,14);
	SetPlayerPos(playerid,258.4893,-41.4008,1002.0234);
	SetPlayerFacingAngle(playerid, 270.0);
	SetPlayerCameraPos(playerid,256.0815,-43.0475,1004.0234);
	SetPlayerCameraLookAt(playerid,258.4893,-41.4008,1002.0234);
	return 1;
}

public OnGameModeInit()
{
	SetGameModeText("Test Script");
	AddPlayerClass(265,1958.3783,1343.1572,15.3746,270.1425,0,0,0,0,-1,-1);
	return 1;
}

public e_COMMAND_ERRORS:OnPlayerCommandPerformed(playerid, cmdtext[], e_COMMAND_ERRORS:success)
{
	if(success != COMMAND_OK)
	{
		new string[128]; 
		format(string, sizeof(string), "{030CDE}[SM] {FF0000}Command %s doesn't exist.", cmdtext);
		SendClientMessage(playerid,-1, string); 
		return COMMAND_OK;
	}
	return COMMAND_OK;
}

YCMD:testcmd(playerid,params[],help)
{
	SendClientMessage(playerid, -1, "Test cmd called and its working ?");
	return 1;
}