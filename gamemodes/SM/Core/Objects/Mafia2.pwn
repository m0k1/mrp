#include <YSI\y_hooks>

hook OnPlayerConnect(playerid)
{
	RemoveBuildingForPlayer(playerid, 708, 162.773, -1249.789, 71.546, 0.250);
	RemoveBuildingForPlayer(playerid, 3733, 175.984, -1307.250, 74.632, 0.250);
	RemoveBuildingForPlayer(playerid, 3737, 169.296, -1332.343, 71.593, 0.250);
	RemoveBuildingForPlayer(playerid, 3604, 169.296, -1332.343, 71.593, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 204.820, -1354.359, 48.679, 0.250);
	RemoveBuildingForPlayer(playerid, 762, 212.773, -1327.968, 74.140, 0.250);
	RemoveBuildingForPlayer(playerid, 762, 209.554, -1318.492, 74.601, 0.250);
	RemoveBuildingForPlayer(playerid, 3607, 175.984, -1307.250, 74.632, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 195.953, -1281.226, 72.750, 0.250);
	RemoveBuildingForPlayer(playerid, 673, 196.375, -1284.937, 73.414, 0.250);
}

hook OnGameModeInit()
{
	print("\n[OnGameModeInit] Initialising 'Objects/Mafia2'...");

	new tmpobjid;
	tmpobjid = CreateDynamicObject(19454,164.251,-1291.688,67.731,0.000,0.000,-0.479);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(19454,164.278,-1289.359,68.091,0.000,0.000,-0.360);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(19454,168.951,-1284.608,68.091,0.000,0.000,90.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(19454,178.553,-1284.605,68.091,0.000,0.000,90.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(19454,183.314,-1284.624,68.091,0.000,0.000,90.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(19454,188.023,-1289.346,68.091,0.000,0.000,-0.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(19454,169.121,-1296.438,67.731,0.000,0.000,89.880);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(18762,176.286,-1307.783,69.023,0.000,90.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(19454,176.138,-1302.618,68.931,0.000,90.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "aamanbev96x", 0);
	tmpobjid = CreateDynamicObject(18762,178.281,-1304.759,69.023,0.000,90.000,90.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(18762,174.306,-1304.775,69.023,0.000,90.000,90.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(18762,174.309,-1299.792,69.023,0.000,90.000,90.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(18762,178.285,-1299.776,69.023,0.000,90.000,90.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 13691, "bevcunto2_lahills", "stonewall3_la", 0);
	tmpobjid = CreateDynamicObject(980,211.051,-1350.360,52.064,0.000,0.000,42.360);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(18762,214.782,-1346.501,52.354,0.000,0.000,38.039);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18762,206.827,-1353.601,47.364,0.000,0.000,43.200);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18762,206.828,-1353.608,52.354,0.000,0.000,43.199);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18766,201.183,-1354.727,48.863,0.000,0.000,17.939);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18766,191.719,-1357.762,53.822,0.000,0.000,17.940);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18766,211.205,-1342.422,52.147,0.000,0.000,-47.819);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18766,210.618,-1341.753,55.525,0.000,0.000,-47.759);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18762,206.141,-1352.879,52.354,0.000,0.000,43.199);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18762,193.425,-1309.770,71.575,0.000,0.000,16.739);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,195.900,-1313.560,74.138,0.000,92.000,17.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10023, "bigwhitesfe", "sfe_arch6", 0);
	tmpobjid = CreateDynamicObject(19454,199.240,-1312.536,74.027,0.000,92.000,17.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,198.680,-1322.775,69.145,0.000,92.000,16.819);
	SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0);
	tmpobjid = CreateDynamicObject(19454,203.431,-1321.302,68.794,0.000,0.000,17.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(18762,199.169,-1308.020,71.555,0.000,0.000,17.340);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,200.706,-1312.092,68.797,0.000,0.000,15.859);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,200.708,-1312.086,72.295,0.000,0.000,15.859);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,202.018,-1321.743,69.025,0.000,92.000,17.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0);
	tmpobjid = CreateDynamicObject(19454,203.431,-1321.302,72.295,0.000,0.000,17.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(18762,204.928,-1325.692,71.555,0.000,0.000,17.340);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(18762,198.791,-1327.497,71.555,0.000,0.000,15.479);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,199.240,-1312.536,69.025,0.000,92.000,17.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0);
	tmpobjid = CreateDynamicObject(19454,195.900,-1313.560,69.165,0.000,92.000,17.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0);
	tmpobjid = CreateDynamicObject(19454,202.044,-1321.739,74.027,0.000,92.000,17.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,198.711,-1322.760,74.138,0.000,92.000,17.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10023, "bigwhitesfe", "sfe_arch6", 0);
	tmpobjid = CreateDynamicObject(18762,202.421,-1316.630,71.555,0.000,0.000,17.340);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,160.582,-1329.094,73.672,0.000,90.000,-0.699);
	SetDynamicObjectMaterial(tmpobjid, 0, 5520, "bdupshouse_lae", "shingles3", 0);
	tmpobjid = CreateDynamicObject(19454,160.720,-1319.474,68.945,0.000,90.000,-1.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0);
	tmpobjid = CreateDynamicObject(19454,164.160,-1319.542,73.656,0.000,90.000,-1.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 5520, "bdupshouse_lae", "shingles3", 0);
	tmpobjid = CreateDynamicObject(19454,164.040,-1329.171,73.678,0.000,90.000,-1.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 5520, "bdupshouse_lae", "shingles3", 0);
	tmpobjid = CreateDynamicObject(18762,165.363,-1325.668,71.230,0.000,0.000,-0.839);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(18762,158.775,-1333.373,71.230,0.000,0.000,-1.200);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(18762,165.506,-1315.285,71.230,0.000,0.000,-0.839);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(18762,158.653,-1315.878,71.230,0.000,0.000,-0.839);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,158.958,-1319.607,67.642,0.000,0.000,-1.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,158.781,-1329.025,71.972,0.000,0.000,-1.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,158.958,-1319.607,71.972,0.000,0.000,-1.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,158.775,-1329.046,67.622,0.000,0.000,-1.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(18762,158.660,-1314.917,71.226,0.000,0.000,-0.839);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(19454,164.199,-1319.546,68.945,0.000,90.000,-1.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0);
	tmpobjid = CreateDynamicObject(19454,164.040,-1329.171,68.945,0.000,90.000,-1.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0);
	tmpobjid = CreateDynamicObject(19454,160.582,-1329.094,68.945,0.000,90.000,-0.699);
	SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0);
	tmpobjid = CreateDynamicObject(19454,160.707,-1319.472,73.672,0.000,90.000,-0.699);
	SetDynamicObjectMaterial(tmpobjid, 0, 5520, "bdupshouse_lae", "shingles3", 0);
	tmpobjid = CreateDynamicObject(18762,165.252,-1333.502,71.170,0.000,0.000,-1.200);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_wallbrick_06", 0);
	tmpobjid = CreateDynamicObject(18766,201.236,-1354.755,53.842,0.000,0.000,17.940);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,199.317,-1285.249,81.189,0.000,0.000,136.319);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,196.076,-1282.153,81.189,0.000,0.000,136.319);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,192.850,-1279.070,81.189,0.000,0.000,136.319);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,189.636,-1276.005,81.189,0.000,0.000,136.319);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,186.398,-1272.906,81.189,0.000,0.000,136.319);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,183.171,-1269.827,81.189,0.000,0.000,136.319);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,179.946,-1266.741,81.189,0.000,0.000,136.319);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,176.709,-1263.669,81.189,0.000,0.000,136.559);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,173.457,-1260.593,81.189,0.000,0.000,136.559);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,170.206,-1257.500,81.189,0.000,0.000,136.559);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,166.957,-1254.440,81.189,0.000,0.000,136.559);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,163.709,-1251.351,81.189,0.000,0.000,136.559);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3049,160.487,-1248.289,81.189,0.000,0.000,136.559);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18766,160.322,-1247.723,67.327,0.000,90.000,-42.060);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18766,160.322,-1247.723,77.237,0.000,90.000,-42.060);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18766,204.022,-1290.070,67.354,0.000,90.000,-46.979);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(18766,204.022,-1290.070,77.242,0.000,90.000,-46.979);
	SetDynamicObjectMaterial(tmpobjid, 0, 16639, "a51_labs", "dam_terazzo", 0);
	tmpobjid = CreateDynamicObject(3928,162.304,-1329.688,73.771,0.000,0.000,-91.019);
	SetDynamicObjectMaterial(tmpobjid, 0, 5520, "bdupshouse_lae", "shingles3", 0);
	tmpobjid = CreateDynamicObject(8572,166.539,-1313.789,69.082,0.000,0.000,-180.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_lattice", 0);
	tmpobjid = CreateDynamicObject(8572,162.061,-1313.781,72.491,0.000,0.000,-180.059);
	SetDynamicObjectMaterial(tmpobjid, 0, 3314, "ce_burbhouse", "sw_lattice", 0);
	tmpobjid = CreateDynamicObject(18762,160.844,-1313.299,70.806,0.000,0.000,-0.839);
	SetDynamicObjectMaterial(tmpobjid, 0, 17508, "barrio1_lae2", "brickred", 0);
	tmpobjid = CreateDynamicObject(18762,159.244,-1313.238,70.806,0.000,0.000,-0.839);
	SetDynamicObjectMaterial(tmpobjid, 0, 17508, "barrio1_lae2", "brickred", 0);
	
	tmpobjid = CreateDynamicObject(9259,176.141,-1292.224,75.007,0.000,0.000,90.479);
	tmpobjid = CreateDynamicObject(747,176.365,-1301.254,69.045,0.000,0.000,-90.360);
	tmpobjid = CreateDynamicObject(9833,175.816,-1303.429,65.846,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(747,176.308,-1305.641,69.045,0.000,0.000,-279.120);
	tmpobjid = CreateDynamicObject(3471,214.497,-1346.133,55.898,0.000,0.000,-50.159);
	tmpobjid = CreateDynamicObject(3471,206.463,-1353.366,55.898,0.000,0.000,-46.439);
	tmpobjid = CreateDynamicObject(19325,196.092,-1308.482,67.864,0.000,0.000,-72.779);
	tmpobjid = CreateDynamicObject(19325,196.092,-1308.482,71.987,0.000,0.000,-72.779);
	tmpobjid = CreateDynamicObject(19325,201.902,-1326.802,67.869,0.000,0.000,-72.779);
	tmpobjid = CreateDynamicObject(19325,201.902,-1326.802,71.987,0.000,0.000,-72.779);
	tmpobjid = CreateDynamicObject(19425,197.654,-1325.608,69.180,16.000,0.000,-72.760);
	tmpobjid = CreateDynamicObject(19425,196.682,-1322.474,69.180,16.000,0.000,-72.760);
	tmpobjid = CreateDynamicObject(19425,196.682,-1322.474,69.180,16.000,0.000,-72.760);
	tmpobjid = CreateDynamicObject(19425,195.703,-1319.323,69.180,16.000,0.000,-72.760);
	tmpobjid = CreateDynamicObject(19425,194.717,-1316.177,69.180,16.000,0.000,-72.760);
	tmpobjid = CreateDynamicObject(19425,193.707,-1313.033,69.180,16.000,0.000,-72.580);
	tmpobjid = CreateDynamicObject(19325,181.805,-1296.038,70.767,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(19325,176.020,-1296.102,70.767,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(19325,170.645,-1296.057,70.767,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(19325,163.558,-1328.369,67.530,0.000,0.000,316.457);
	tmpobjid = CreateDynamicObject(19325,163.558,-1328.369,71.648,0.000,0.000,316.457);
	tmpobjid = CreateDynamicObject(19325,159.866,-1332.261,70.383,90.000,0.000,316.457);
	tmpobjid = CreateDynamicObject(19325,161.972,-1314.882,67.541,0.000,0.000,268.757);
	tmpobjid = CreateDynamicObject(19325,158.760,-1330.179,68.339,0.000,0.000,359.057);
	tmpobjid = CreateDynamicObject(19325,158.866,-1323.739,68.339,0.000,0.000,359.057);
	tmpobjid = CreateDynamicObject(19325,158.970,-1318.382,70.383,90.000,0.000,358.637);
	tmpobjid = CreateDynamicObject(19325,162.133,-1314.885,71.648,0.000,0.000,268.757);
	tmpobjid = CreateDynamicObject(19454,164.267,-1314.809,64.100,90.000,90.000,-0.819);
	tmpobjid = CreateDynamicObject(19454,160.759,-1314.739,64.100,90.000,90.000,-0.819);
	tmpobjid = CreateDynamicObject(19425,166.121,-1316.402,68.900,34.000,0.000,-271.000);
	tmpobjid = CreateDynamicObject(19425,166.041,-1319.688,68.900,34.000,0.000,-271.000);
	tmpobjid = CreateDynamicObject(19425,165.967,-1322.972,68.900,34.000,0.000,-271.000);

}