#include <YSI\y_hooks>

hook OnPlayerConnect(playerid)
{
	RemoveBuildingForPlayer(playerid, 5578, 2049.867, -1400.890, 20.679, 0.250);
	RemoveBuildingForPlayer(playerid, 5579, 2050.070, -1401.210, 33.679, 0.250);
	RemoveBuildingForPlayer(playerid, 5636, 2042.179, -1346.804, 24.007, 0.250);
	RemoveBuildingForPlayer(playerid, 1525, 2093.757, -1413.445, 24.117, 0.250);
	RemoveBuildingForPlayer(playerid, 5661, 2050.070, -1401.210, 33.679, 0.250);
	RemoveBuildingForPlayer(playerid, 5467, 2026.117, -1404.640, 46.000, 0.250);
	RemoveBuildingForPlayer(playerid, 5403, 2050.070, -1401.210, 33.679, 0.250);
	RemoveBuildingForPlayer(playerid, 5402, 2049.867, -1400.890, 20.679, 0.250);
	RemoveBuildingForPlayer(playerid, 673, 2008.257, -1433.992, 12.031, 0.250);
	RemoveBuildingForPlayer(playerid, 645, 2009.078, -1424.976, 14.078, 0.250);
	RemoveBuildingForPlayer(playerid, 700, 2022.453, -1439.968, 14.453, 0.250);
	RemoveBuildingForPlayer(playerid, 673, 2024.007, -1431.656, 12.812, 0.250);
}

hook OnGameModeInit()
{
	print("\n[OnGameModeInit] Initialising 'Objects/Bolnica'...");
	new tmpobjid;
	tmpobjid = CreateDynamicObject(5402,2049.858,-1400.899,20.637,0.000,0.000,-0.019,-1,-1,-1,500,500);
	SetDynamicObjectMaterial(tmpobjid, 0, 7509, "vgwestretail1", "hedge2_256", 0);
	SetDynamicObjectMaterial(tmpobjid, 1, 8130, "vgsschurch", "vgschurchwall04_256", 0);
	SetDynamicObjectMaterial(tmpobjid, 2, 8130, "vgsschurch", "vgschurchwall04_256", 0);
	SetDynamicObjectMaterial(tmpobjid, 3, 7488, "vegasdwntwn1", "vgnstonewall1_256", 0);
	SetDynamicObjectMaterial(tmpobjid, 4, 7509, "vgwestretail1", "hedge2_256", 0);
	tmpobjid = CreateDynamicObject(5403,2050.065,-1401.215,33.599,0.000,0.000,0.000,-1,-1,-1,500,500);
	SetDynamicObjectMaterial(tmpobjid, 0, 6060, "shops2_law", "biffoffwin_law", 0);
	SetDynamicObjectMaterial(tmpobjid, 1, 6060, "shops2_law", "biffoffwin_law", 0);
	SetDynamicObjectMaterial(tmpobjid, 2, 5040, "shopliquor_las", "lasjmliq1", 0);
	SetDynamicObjectMaterial(tmpobjid, 3, 6095, "shops01_law", "GB_shopdoor01", 0);
	tmpobjid = CreateDynamicObject(19454,2017.873,-1434.701,11.859,0.000,0.000,-47.340);
	SetDynamicObjectMaterial(tmpobjid, 0, 6060, "shops2_law", "newall8-1blue", 0);
	tmpobjid = CreateDynamicObject(19356,2013.138,-1439.022,11.865,0.000,0.000,-47.880);
	SetDynamicObjectMaterial(tmpobjid, 0, 6060, "shops2_law", "newall8-1blue", 0);
	tmpobjid = CreateDynamicObject(19454,2013.404,-1430.618,11.859,0.000,0.000,-46.920);
	SetDynamicObjectMaterial(tmpobjid, 0, 6060, "shops2_law", "newall8-1blue", 0);
	tmpobjid = CreateDynamicObject(19356,2008.727,-1435.020,11.865,0.000,0.000,-46.679);
	SetDynamicObjectMaterial(tmpobjid, 0, 6060, "shops2_law", "newall8-1blue", 0);
	tmpobjid = CreateDynamicObject(19089,2032.496,-1401.333,18.645,0.000,0.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	SetDynamicObjectMaterial(tmpobjid, 1, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2035.474,-1401.348,18.645,0.000,0.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2042.855,-1401.342,16.290,0.000,90.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2032.503,-1401.338,15.581,0.000,89.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2025.130,-1401.341,16.170,0.000,90.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2032.503,-1401.338,16.290,0.000,89.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2032.483,-1401.340,16.250,0.000,89.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2020.855,-1407.719,16.130,0.000,90.000,-89.939);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2030.866,-1407.720,16.130,0.000,90.000,0.180);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2023.494,-1407.751,16.130,0.000,90.000,0.180);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2038.413,-1408.068,16.170,0.000,91.000,-90.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2038.458,-1401.336,20.920,0.000,0.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2020.841,-1401.347,20.955,0.000,0.000,0.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2030.866,-1407.720,16.150,0.000,91.000,-104.199);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2034.549,-1409.350,16.130,0.000,91.000,-104.199);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(19089,2036.362,-1402.232,16.270,0.000,91.000,-90.000);
	SetDynamicObjectMaterial(tmpobjid, 0, 10765, "airportgnd_sfse", "black64", 0);
	tmpobjid = CreateDynamicObject(1251,2041.831,-1414.187,16.116,0.000,0.000,116.880);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);
	tmpobjid = CreateDynamicObject(1251,2041.910,-1418.852,16.116,0.000,0.000,116.880);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);
	tmpobjid = CreateDynamicObject(1251,2041.882,-1423.916,16.116,0.000,0.000,116.880);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);
	tmpobjid = CreateDynamicObject(1251,2041.900,-1430.087,16.116,0.000,0.000,116.880);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);
	tmpobjid = CreateDynamicObject(1251,2040.235,-1410.328,16.116,0.000,0.000,119.100);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);
	tmpobjid = CreateDynamicObject(1251,2020.149,-1418.434,15.936,0.000,0.000,130.859);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);
	tmpobjid = CreateDynamicObject(1251,2025.928,-1418.337,15.936,0.000,0.000,132.779);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);
	tmpobjid = CreateDynamicObject(1251,2014.244,-1418.447,15.936,0.000,0.000,130.859);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);
	tmpobjid = CreateDynamicObject(1251,2008.747,-1418.536,15.936,0.000,0.000,130.859);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);
	tmpobjid = CreateDynamicObject(1251,2030.236,-1422.913,15.936,0.000,0.000,133.439);
	SetDynamicObjectMaterial(tmpobjid, 0, 1975, "texttest", "kb_red", 0);

	tmpobjid = CreateDynamicObject(19425,1998.663,-1450.859,12.491,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(19425,2001.966,-1450.860,12.491,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(19425,2005.267,-1450.868,12.491,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(19425,2008.568,-1450.869,12.491,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(19425,1996.968,-1449.000,12.491,0.000,0.000,90.000);
	tmpobjid = CreateDynamicObject(19425,1996.974,-1445.700,12.491,0.000,0.000,90.000);
	tmpobjid = CreateDynamicObject(19425,1996.962,-1442.419,12.491,0.000,0.000,90.000);
	tmpobjid = CreateDynamicObject(19425,1996.949,-1439.121,12.491,0.000,0.000,90.000);
	tmpobjid = CreateDynamicObject(19425,1999.302,-1350.826,22.906,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(19425,2002.602,-1350.819,22.906,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(18762,2038.912,-1434.238,16.141,0.000,90.000,90.000);
	tmpobjid = CreateDynamicObject(18762,2038.913,-1439.230,16.121,0.000,90.000,90.000);
	tmpobjid = CreateDynamicObject(18762,2038.901,-1442.486,16.141,0.000,90.000,90.000);
	tmpobjid = CreateDynamicObject(18762,2041.011,-1444.484,16.641,0.000,89.000,-0.779);
	tmpobjid = CreateDynamicObject(18762,2045.992,-1444.549,17.121,0.000,89.000,-0.779);
	tmpobjid = CreateDynamicObject(18762,2050.776,-1444.614,17.633,0.000,89.000,-0.779);
	tmpobjid = CreateDynamicObject(18762,2055.658,-1444.686,18.191,0.000,89.000,-0.779);
	tmpobjid = CreateDynamicObject(19425,2102.597,-1448.775,22.933,0.000,0.000,90.000);
	tmpobjid = CreateDynamicObject(19425,2102.598,-1445.472,22.933,0.000,0.000,90.000);
	tmpobjid = CreateDynamicObject(1232,2021.699,-1450.712,16.605,356.858,0.000,3.141);
	tmpobjid = CreateDynamicObject(14400,2020.143,-1435.475,12.984,0.000,0.000,58.139);
	tmpobjid = CreateDynamicObject(14400,2023.925,-1431.994,12.984,0.000,0.000,58.139);
	tmpobjid = CreateDynamicObject(14400,2012.321,-1428.481,12.984,0.000,0.000,38.159);
	tmpobjid = CreateDynamicObject(14400,2016.873,-1424.061,12.984,0.000,0.000,38.159);
	tmpobjid = CreateDynamicObject(747,2016.900,-1438.100,12.744,0.000,0.000,44.159);
	tmpobjid = CreateDynamicObject(747,2009.696,-1431.155,12.824,0.000,0.000,32.400);
	tmpobjid = CreateDynamicObject(747,2007.612,-1433.431,12.824,0.000,0.000,-120.239);
	tmpobjid = CreateDynamicObject(747,2014.446,-1440.128,12.744,0.000,0.000,-157.199);
	tmpobjid = CreateDynamicObject(5467,2026.047,-1404.177,45.806,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(1569,2032.492,-1401.280,16.143,0.000,0.000,0.000);
	tmpobjid = CreateDynamicObject(1569,2035.495,-1401.284,16.143,0.000,0.000,179.879);
	tmpobjid = CreateDynamicObject(18762,2041.771,-1432.208,16.141,0.000,90.000,0.719);
	tmpobjid = CreateDynamicObject(18762,2044.636,-1411.861,16.141,0.000,90.000,90.000);
	tmpobjid = CreateDynamicObject(18762,2044.634,-1416.858,16.141,0.000,90.000,90.000);
	tmpobjid = CreateDynamicObject(18762,2044.636,-1421.863,16.141,0.000,90.000,90.000);
	tmpobjid = CreateDynamicObject(18762,2044.633,-1426.846,16.141,0.000,90.000,90.000);
	tmpobjid = CreateDynamicObject(18762,2044.639,-1431.830,16.141,0.000,90.000,90.000);
	tmpobjid = CreateDynamicObject(1280,2019.768,-1432.340,12.730,0.000,0.000,-45.239);
	tmpobjid = CreateDynamicObject(1280,2015.778,-1436.076,12.730,0.000,0.000,-45.899);
	tmpobjid = CreateDynamicObject(1280,2012.403,-1432.278,12.730,0.000,0.000,-226.259);
	tmpobjid = CreateDynamicObject(1280,2016.267,-1428.650,12.730,0.000,0.000,-225.420);
	tmpobjid = CreateDynamicObject(776,2023.549,-1436.863,12.598,0.000,0.000,-356.100);
	tmpobjid = CreateDynamicObject(776,2010.921,-1424.309,12.598,0.000,0.000,-537.180);
}