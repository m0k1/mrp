#include <YSI\y_hooks>

hook OnGameModeInit()
{
	print("\n[OnGameModeInit] Initialising 'Objects/Lutrija'...");

	CreateDynamicObject(1569, 1730.61951, -1264.61682, 13.11814,   0.00000, 0.00000, 49.08002);
	CreateDynamicObject(2773, 1732.04297, -1271.74536, 12.54693,   0.00000, 0.00000, -40.32001);
	CreateDynamicObject(2773, 1728.27966, -1268.48596, 12.54693,   0.00000, 0.00000, -40.32001);
	CreateDynamicObject(948, 1732.94104, -1271.72266, 12.54862,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(948, 1728.34387, -1267.71411, 12.54862,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2257, 1730.58215, -1264.91565, 14.57448,   0.00000, 0.00000, 47.82000);
	CreateDynamicObject(2256, 1735.34790, -1269.19507, 14.60461,   0.00000, 0.00000, -131.88011);
	CreateDynamicObject(2165, 1733.38245, -1268.81726, 12.68396,   0.00000, 0.00000, -223.01993);
	CreateDynamicObject(2165, 1731.87695, -1267.48242, 12.68396,   0.00000, 0.00000, -223.01993);
	CreateDynamicObject(2186, 1732.60767, -1264.38904, 12.68472,   0.00000, 0.00000, -41.40000);
	CreateDynamicObject(2164, 1734.29810, -1265.26172, 12.68448,   0.00000, 0.00000, -41.82000);
	CreateDynamicObject(2164, 1735.25134, -1266.11646, 12.68448,   0.00000, 0.00000, -41.82000);
	CreateDynamicObject(1714, 1732.53552, -1265.99780, 12.68422,   0.00000, 0.00000, -50.52000);
	CreateDynamicObject(1714, 1733.89709, -1267.38086, 12.68422,   0.00000, 0.00000, -50.52000);
	CreateDynamicObject(19325, 1731.08936, -1269.18591, 15.22490,   0.00000, 0.00000, 48.59999);

	new smlutrija[22];

	smlutrija[1] = CreateDynamicObject(18762, 1732.33594, -1263.14478, 14.05669,   0.00000, 0.00000, -41.51999);
	smlutrija[2] = CreateDynamicObject(18762, 1736.93066, -1267.18152, 14.05669,   0.00000, 0.00000, -41.51999);
	smlutrija[3] = CreateDynamicObject(18762, 1730.41541, -1265.36707, 12.63760,   0.00000, 90.00000, -131.76003);
	smlutrija[4] = CreateDynamicObject(18762, 1734.91479, -1269.31458, 12.63760,   0.00000, 90.00000, -131.76003);
	smlutrija[5] = CreateDynamicObject(18762, 1731.33130, -1268.83228, 12.63760,   0.00000, 90.00000, -221.45990);
	smlutrija[6] = CreateDynamicObject(18762, 1729.11829, -1266.85168, 14.05669,   0.00000, 0.00000, -41.51999);
	smlutrija[7] = CreateDynamicObject(18762, 1733.58020, -1270.77429, 14.05669,   0.00000, 0.00000, -41.51999);
	smlutrija[8] = CreateDynamicObject(18762, 1730.45117, -1265.34351, 16.79450,   0.00000, 90.00000, -131.76001);
	smlutrija[9] = CreateDynamicObject(18762, 1733.74402, -1261.64441, 16.79450,   0.00000, 90.00000, -131.76001);
	smlutrija[10] = CreateDynamicObject(18762, 1734.92200, -1269.27136, 16.79450,   0.00000, 90.00000, -131.76001);
	smlutrija[11] = CreateDynamicObject(18762, 1738.18774, -1265.63281, 16.79450,   0.00000, 90.00000, -131.76001);
	smlutrija[12] = CreateDynamicObject(18762, 1731.37036, -1268.78674, 16.80989,   0.00000, 90.00000, -221.45990);
	smlutrija[13] = CreateDynamicObject(18762, 1734.70020, -1265.02917, 16.80989,   0.00000, 90.00000, -221.45990);
	smlutrija[14] = CreateDynamicObject(19445, 1732.28064, -1262.85718, 14.61307,   0.00000, 0.00000, -41.75999);
	smlutrija[15] = CreateDynamicObject(19445, 1736.88525, -1267.62427, 14.61307,   0.00000, 0.00000, -41.75999);
	smlutrija[16] = CreateDynamicObject(19464, 1731.12646, -1269.09961, 10.95619,   0.00000, 0.00000, 48.53998);
	smlutrija[17] = CreateDynamicObject(19464, 1734.85571, -1265.34180, 13.97980,   0.00000, 0.00000, 48.53998);
	smlutrija[18] = CreateDynamicObject(19464, 1730.81067, -1268.74438, 12.46733,   0.00000, 90.00000, 48.54000);
	smlutrija[19] = CreateDynamicObject(19464, 1731.52734, -1269.38220, 12.47814,   0.00000, 90.00000, 48.54000);
	smlutrija[20] = CreateDynamicObject(19464, 1732.72693, -1266.87329, 12.55788,   0.00000, 90.00000, 48.54000);
	smlutrija[21] = CreateDynamicObject(19464, 1732.72693, -1266.87329, 17.12621,   0.00000, 90.00000, 48.54000);

	SetDynamicObjectMaterial(smlutrija[1], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[2], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[3], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[4], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[5], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[6], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[7], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[8], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[9], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[10], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[11], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[12], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[13], 0, 16377, "des_byofficeint", "CJ_WOOD5", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[14], 0, 6095, "shops01_law", "GB_chatwall03b", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[15], 0, 6095, "shops01_law", "GB_chatwall03b", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[16], 0, 6095, "shops01_law", "GB_chatwall03b", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[17], 0, 6095, "shops01_law", "GB_chatwall03b", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[18], 0, 8399, "vgs_shops", "vgsclubwall05_128", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[19], 0, 8399, "vgs_shops", "vgsclubwall05_128", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[20], 0, 16150, "ufo_bar", "sa_wood08_128", 0xFFFFFFFF);
	SetDynamicObjectMaterial(smlutrija[21], 0, 16150, "ufo_bar", "sa_wood08_128", 0xFFFFFFFF);

	new LUTRIJASM = CreateDynamicObject(19353, 1731.16418, -1269.0, 17.01062,   0.00000, 0.00000, 48.60002);
	SetDynamicObjectMaterialText(LUTRIJASM, 0,"-LUTRIJA-",  130, "ARIAL BLACK", 120, 1, -65536, 0, 1);
}