// Map Mover 1.1 by adri1

#define MAX_MAPMOVER_OBJECTS    (500)
#define MAX_MAPMOVER_REMOVEBUILDING 1





//-------------------------------------------------
//
// This is an example of using the AttachCameraToObject function
// to create a no-clip flying camera.
//
// h02 2012
//
// SA-MP 0.3e and above
//
//-------------------------------------------------

#include <a_samp>
#include <sscanf2>

// Players Move Speed
#define MOVE_SPEED              100.0
#define ACCEL_RATE              0.03

// Players Mode
#define CAMERA_MODE_NONE    	0
#define CAMERA_MODE_FLY     	1

// Key state definitions
#define MOVE_FORWARD    		1
#define MOVE_BACK       		2
#define MOVE_LEFT       		3
#define MOVE_RIGHT      		4
#define MOVE_FORWARD_LEFT       5
#define MOVE_FORWARD_RIGHT      6
#define MOVE_BACK_LEFT          7
#define MOVE_BACK_RIGHT         8

// Enumeration for storing data about the player
enum noclipenum
{
	cameramode,
	flyobject,
	mode,
	lrold,
	udold,
	lastmove,
	Float:accelmul
}
new noclipdata[MAX_PLAYERS][noclipenum];

//--------------------------------------------------

public OnFilterScriptInit()
{
	return 1;
}

public OnFilterScriptExit()
{
	// If any players are still in edit mode, boot them out before the filterscript unloads
	for(new x; x<MAX_PLAYERS; x++)
	{
		if(noclipdata[x][cameramode] == CAMERA_MODE_FLY) CancelFlyMode(x);
	}
	return 1;
}

//--------------------------------------------------

public OnPlayerConnect(playerid)
{
	// Reset the data belonging to this player slot
	noclipdata[playerid][cameramode] 	= CAMERA_MODE_NONE;
	noclipdata[playerid][lrold]	   	 	= 0;
	noclipdata[playerid][udold]   		= 0;
	noclipdata[playerid][mode]   		= 0;
	noclipdata[playerid][lastmove]   	= 0;
	noclipdata[playerid][accelmul]   	= 0.0;
	return 1;
}


/*          CODE        */


new Object_String[512];
new Player_MovingMap[MAX_PLAYERS];
new Player_MovingObjects[MAX_PLAYERS];
new ObjectCenter;
new mapnamefile[24];
new Float:CamOffSetX;
new Float:CamOffSetY;
new Float:CamOffSetZ;
enum OBJECT_INFORMATION
{
	ModelID,
	Float:PositionX,
	Float:PositionY,
	Float:PositionZ,
	Float:RotationX,
	Float:RotationY,
	Float:RotationZ,
	Float:OffSetX,
	Float:OffSetY,
	Float:OffSetZ,
	ObjectID
};
new MAPMOVER_OBJECTS[MAX_MAPMOVER_OBJECTS][OBJECT_INFORMATION];

stock LoadMap(playerid, mapname[])
{
	if(Player_MovingMap[playerid] == 0) return 1;
    new File:Handler = fopen(mapname, io_read);
    if(!Handler) return ShowPlayerDialog(playerid, 801, DIALOG_STYLE_MSGBOX, "Map Mover", "\n\n\tThe map could not load:\n\tFile not found.\n\n", "OK", "");
    new Index;
    while(fread(Handler, Object_String))
    {
        StripNewLine(Object_String);
        new t = 0;
        if(strfind(Object_String, "CreateObject(", true) != -1) strdel(Object_String, 0, 13), t = 1;
        if(strfind(Object_String, "CreateDynamicObject(", true) != -1) strdel(Object_String, 0, 20), t = 1;
        #if MAX_MAPMOVER_REMOVEBUILDING 1
		if(strfind(Object_String, "RemoveBuildingForPlayer(playerid,", true) != -1) strdel(Object_String, 0, 33), t = 2;
		#endif
        if(strfind(Object_String, ");", true) != -1)
		{
		    new start = strfind(Object_String, ");", true);
            strdel(Object_String, start, 500);
		}
		if(t == 1)
		{
			if(!sscanf(Object_String, "p<,>dffffff",
			MAPMOVER_OBJECTS[Index][ModelID],
			MAPMOVER_OBJECTS[Index][PositionX], MAPMOVER_OBJECTS[Index][PositionY], MAPMOVER_OBJECTS[Index][PositionZ],
			MAPMOVER_OBJECTS[Index][RotationX], MAPMOVER_OBJECTS[Index][RotationY], MAPMOVER_OBJECTS[Index][RotationZ]))
			{

			    new objectid = CreateObject(MAPMOVER_OBJECTS[Index][ModelID],
											MAPMOVER_OBJECTS[Index][PositionX], MAPMOVER_OBJECTS[Index][PositionY], MAPMOVER_OBJECTS[Index][PositionZ],
											MAPMOVER_OBJECTS[Index][RotationX], MAPMOVER_OBJECTS[Index][RotationY], MAPMOVER_OBJECTS[Index][RotationZ]);
	            MAPMOVER_OBJECTS[Index][ObjectID] = objectid;
			    Index++;
			}
		}
		else if(t == 2)
		{
		    new m, Float:x, Float:y, Float:z, Float:r;
		    if(!sscanf(Object_String, "p<,>dffff", m, x, y, z, r)) RemoveBuildingForPlayer(playerid, m, x, y, z, r);
		}
    }
    fclose(Handler);

	if(Index == 0) ShowPlayerDialog(playerid, 801, DIALOG_STYLE_MSGBOX, "Map Mover", "\n\n\tCan't find any map in this file.\n\n", "OK", "");
	else
	{
		ShowPlayerDialog(playerid, 802, DIALOG_STYLE_MSGBOX, "Map Mover", "\n\n\tMap loaded correctly.\n\n", "OK", "");
		format(mapnamefile, 24, "%s", mapname);
	}
	return true;
}



stock StripNewLine(string[]) //DracoBlue (bugfix idea by Y_Less)
{
	new len = strlen(string);
	if (string[0]==0) return ;
	if ((string[len - 1] == '\n') || (string[len - 1] == '\r')) {
		string[len - 1] = 0;
		if (string[0]==0) return ;
		if ((string[len - 2] == '\n') || (string[len - 2] == '\r')) string[len - 2] = 0;
	}
}

public OnPlayerDisconnect(playerid, reason)
{
    if(Player_MovingMap[playerid] == 1)
    {
        for(new i = 0; i != MAX_MAPMOVER_OBJECTS; i++)
        {
            if(MAPMOVER_OBJECTS[i][ModelID] != 0)
            {
                DestroyObject(MAPMOVER_OBJECTS[i][ObjectID]);
                MAPMOVER_OBJECTS[i][ModelID] = 0;
                MAPMOVER_OBJECTS[i][PositionX] = 0.0;
                MAPMOVER_OBJECTS[i][PositionY] = 0.0;
                MAPMOVER_OBJECTS[i][PositionZ] = 0.0;
                MAPMOVER_OBJECTS[i][RotationX] = 0.0;
                MAPMOVER_OBJECTS[i][RotationY] = 0.0;
                MAPMOVER_OBJECTS[i][RotationZ] = 0.0;
                MAPMOVER_OBJECTS[i][OffSetX] = 0.0;
                MAPMOVER_OBJECTS[i][OffSetY] = 0.0;
                MAPMOVER_OBJECTS[i][OffSetZ] = 0.0;
                MAPMOVER_OBJECTS[i][ObjectID] = 0;
            }
		}
		DestroyObject(ObjectCenter);
		ObjectCenter = 0;
		Player_MovingObjects[playerid] = 0;
		Player_MovingMap[playerid] = 0;
    }
	return 1;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
	if(!strcmp(cmdtext, "/map", true))
	{
		if(GetPVarType(playerid, "FlyMode"))
		{
		    if(Player_MovingObjects[playerid] >= 1) return SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}Finish move the map before showing the menu.");
   			if(Player_MovingMap[playerid] == 1) ShowPlayerDialog(playerid, 803, DIALOG_STYLE_LIST, "Map Mover", "1. Locate\n2. Center position\n3. Edit positions\n4. Export map\n5. Exit (without saving)", "OK", "");
		}
		else
		{
            if(Player_MovingMap[playerid] == 1) return 1;
            Player_MovingMap[playerid] = 1;
		    ShowPlayerDialog(playerid, 800, DIALOG_STYLE_INPUT, "Map Mover", "\n\nEnter the file name .txt with the map\nthat has been saved in the folder scriptfiles.\n\n\t(You must enter the extension .txt)\n\n", "Load", "X");
			FlyMode(playerid);
		}
		return 1;
	}
	return 0;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
    if(dialogid == 800)
    {
        if(response)
        {
            ShowPlayerDialog(playerid, 801, DIALOG_STYLE_MSGBOX, "Map Mover", "\n\n\tLoading Map...\n\n", "OK", "");
            LoadMap(playerid, inputtext);
        }
        else
        {
            Player_MovingMap[playerid] = 0;
            CancelFlyMode(playerid);
        }
    }
    if(dialogid == 801) ShowPlayerDialog(playerid, 800, DIALOG_STYLE_INPUT, "Map Mover", "\n\nEnter the file name .txt with the map\nthat has been saved in the folder scriptfiles.\n\n\t(You must enter the extension .txt)\n\n", "Load", "X");
    if(dialogid == 802) ShowPlayerDialog(playerid, 803, DIALOG_STYLE_LIST, "Map Mover", "1. Locate\n2. Center position\n3. Edit positions\n4. Export map\n5. Exit (without saving)", "OK", "");
    if(dialogid == 803)
    {
        if(response)
        {
            switch(listitem)
            {
	            case 0:
	            {
	                if(ObjectCenter == 0)
	                {
						new Float:CameraX = MAPMOVER_OBJECTS[0][PositionX];
						new Float:CameraY = MAPMOVER_OBJECTS[0][PositionY];
						new Float:CameraZ = MAPMOVER_OBJECTS[0][PositionZ];
                        SetPlayerObjectPos(playerid, noclipdata[playerid][flyobject], CameraX, CameraY, CameraZ);
                        SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}Use the {FFFF00}/map {FFFFFF}command again to show the main menu.");
	                }
	                else
	                {
	                    new Float:CameraX;
						new Float:CameraY;
						new Float:CameraZ;
						GetObjectPos(ObjectCenter, CameraX, CameraY, CameraZ);
                        SetPlayerObjectPos(playerid, noclipdata[playerid][flyobject], CameraX, CameraY, CameraZ);
                        SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}Use the {FFFF00}/map {FFFFFF}command again to show the main menu.");
	                }
				}
				case 1:
				{
				    if(ObjectCenter == 0)
	                {
	                    new Float:CameraX;
						new Float:CameraY;
						new Float:CameraZ;
						GetPlayerObjectPos(playerid, noclipdata[playerid][flyobject], CameraX, CameraY, CameraZ);
	                    ObjectCenter = CreateObject(1220, CameraX, CameraY, CameraZ, 0.0, 0.0, 0.0);
	                    SetObjectMaterial(ObjectCenter, 0, 18646, "matcolours", "white");
	                    EditObject(playerid, ObjectCenter);
	                    SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}Place your object in the center of the map loaded.");
	                }
	                else
	                {
	                    SetObjectMaterial(ObjectCenter, 0, 18646, "matcolours", "white");
	                    EditObject(playerid, ObjectCenter);
	                    SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}Place your object in the center of the map loaded.");
	                }
				}
				case 2:
				{
				    if(ObjectCenter == 0) return SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}You must place the center first.");
				    ShowPlayerDialog(playerid, 804, DIALOG_STYLE_LIST, "Map Mover - Move options", "1. Move with cursor (EditObject)\n2. Move with camera (new)", "OK", "<<");
				}
				case 3:
				{
				    if(ObjectCenter == 0) return SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}Not noticed any change.");
				    new f_name[24];
					format(f_name, 24, "%s_converted.txt", mapnamefile);
					new File:codefile = fopen(f_name, io_write);
					if(codefile)
					{
					    new Year, Month, Day;
						getdate(Year, Month, Day);
						new Hour, Minute, Second;
						gettime(Hour, Minute, Second);
					    new intro[128]; format(intro, 128, "Map Mover ~ %s ~ %02d/%02d/%d ~ %02d:%02d:%02d\r\n\r\n", mapnamefile, Day, Month, Year, Hour, Minute, Second);
					    fwrite(codefile, intro);
					    for(new i = 0; i != MAX_MAPMOVER_OBJECTS; i++)
				        {
				            if(MAPMOVER_OBJECTS[i][ModelID] != 0)
				            {
				                new line[128];
								format(line, 128, "CreateObject(%d, %f, %f, %f, %f, %f, %f);\r\n", MAPMOVER_OBJECTS[i][ModelID], MAPMOVER_OBJECTS[i][PositionX], MAPMOVER_OBJECTS[i][PositionY], MAPMOVER_OBJECTS[i][PositionZ], MAPMOVER_OBJECTS[i][RotationX], MAPMOVER_OBJECTS[i][RotationY], MAPMOVER_OBJECTS[i][RotationZ]);
                                fwrite(codefile, line);
							}
						}
						fwrite(codefile, "\r\n");
						fwrite(codefile, "\r\n");
						fwrite(codefile, "RemoveBuildings no converted (not necessary).");
						fwrite(codefile, "Map Mover 1.1 by adri1.");
					    fclose(codefile);
					}

				}
				case 4:
				{
    				for(new i = 0; i != MAX_MAPMOVER_OBJECTS; i++)
			        {
			            if(MAPMOVER_OBJECTS[i][ModelID] != 0)
			            {
			                DestroyObject(MAPMOVER_OBJECTS[i][ObjectID]);
			                MAPMOVER_OBJECTS[i][ModelID] = 0;
			                MAPMOVER_OBJECTS[i][PositionX] = 0.0;
			                MAPMOVER_OBJECTS[i][PositionY] = 0.0;
			                MAPMOVER_OBJECTS[i][PositionZ] = 0.0;
			                MAPMOVER_OBJECTS[i][RotationX] = 0.0;
			                MAPMOVER_OBJECTS[i][RotationY] = 0.0;
			                MAPMOVER_OBJECTS[i][RotationZ] = 0.0;
			                MAPMOVER_OBJECTS[i][OffSetX] = 0.0;
			                MAPMOVER_OBJECTS[i][OffSetY] = 0.0;
			                MAPMOVER_OBJECTS[i][OffSetZ] = 0.0;
			                MAPMOVER_OBJECTS[i][ObjectID] = 0;
			            }
					}
					DestroyObject(ObjectCenter);
					Player_MovingObjects[playerid] = 0;
					Player_MovingMap[playerid] = 0;
					ObjectCenter = 0;
					CancelFlyMode(playerid);
					CancelEdit(playerid);
				}
			}
        }
        else SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}Use the {FFFF00}/map {FFFFFF}command again to show the main menu.");
	}
	if(dialogid == 804)
	{
	    if(response)
	    {
	        switch(listitem)
	        {
	            case 0:
	            {
	                new Float:CenterX, Float:CenterY, Float:CenterZ;
				    GetObjectPos(ObjectCenter, CenterX, CenterY, CenterZ);
                    for(new i = 0; i != MAX_MAPMOVER_OBJECTS; i++)
                    {
                        if(MAPMOVER_OBJECTS[i][ModelID] != 0)
                        {
							new Float:aOffSetX, Float:aOffSetY, Float:aOffSetZ;
							aOffSetX = floatsub(MAPMOVER_OBJECTS[i][PositionX], CenterX);
							aOffSetY = floatsub(MAPMOVER_OBJECTS[i][PositionY], CenterY);
							aOffSetZ = floatsub(MAPMOVER_OBJECTS[i][PositionZ], CenterZ);
                            MAPMOVER_OBJECTS[i][OffSetX] = aOffSetX;
                            MAPMOVER_OBJECTS[i][OffSetY] = aOffSetY;
                            MAPMOVER_OBJECTS[i][OffSetZ] = aOffSetZ;
							AttachObjectToObject(MAPMOVER_OBJECTS[i][ObjectID], ObjectCenter, aOffSetX, aOffSetY, aOffSetZ, MAPMOVER_OBJECTS[i][RotationX], MAPMOVER_OBJECTS[i][RotationY], MAPMOVER_OBJECTS[i][RotationZ], true);
                        }
                    }
                    Player_MovingObjects[playerid] = 1;
                    EditObject(playerid, ObjectCenter);
	            }
	            case 1:
	            {
	                SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}Move the camera to move the map. Press {FFFF00}~k~~PED_DUCK~ {FFFFFF}for stop.");
	                new Float:CenterX, Float:CenterY, Float:CenterZ;
				    GetObjectPos(ObjectCenter, CenterX, CenterY, CenterZ);
                    for(new i = 0; i != MAX_MAPMOVER_OBJECTS; i++)
                    {
                        if(MAPMOVER_OBJECTS[i][ModelID] != 0)
                        {
							new Float:aOffSetX, Float:aOffSetY, Float:aOffSetZ;
							aOffSetX = floatsub(MAPMOVER_OBJECTS[i][PositionX], CenterX);
							aOffSetY = floatsub(MAPMOVER_OBJECTS[i][PositionY], CenterY);
							aOffSetZ = floatsub(MAPMOVER_OBJECTS[i][PositionZ], CenterZ);
                            MAPMOVER_OBJECTS[i][OffSetX] = aOffSetX;
                            MAPMOVER_OBJECTS[i][OffSetY] = aOffSetY;
                            MAPMOVER_OBJECTS[i][OffSetZ] = aOffSetZ;
							AttachObjectToObject(MAPMOVER_OBJECTS[i][ObjectID], ObjectCenter, aOffSetX, aOffSetY, aOffSetZ, MAPMOVER_OBJECTS[i][RotationX], MAPMOVER_OBJECTS[i][RotationY], MAPMOVER_OBJECTS[i][RotationZ], true);
						}
                    }
                    new Float:fVX, Float:fVY, Float:fVZ, Float:object_x, Float:object_y, Float:object_z;
					const Float:fScale = 50.0;
					GetPlayerCameraFrontVector(playerid, fVX, fVY, fVZ);
					object_x = floatmul(fVX, fScale);
					object_y = floatmul(fVY, fScale);
					object_z = floatmul(fVZ, fScale);
					AttachObjectToObject(ObjectCenter, noclipdata[playerid][flyobject], object_x, object_y, object_z, 0.0, 0.0, 0.0, true);
					CamOffSetX = object_x;
					CamOffSetY = object_y;
					CamOffSetZ = object_z;
                    Player_MovingObjects[playerid] = 2;
	            }
			}
	    }
	    else ShowPlayerDialog(playerid, 803, DIALOG_STYLE_LIST, "Map Mover", "1. Locate\n2. Center position\n3. Edit positions\n4. Export map\n5. Exit (without saving)", "OK", "");
	}
    return 0;
}

public OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if((newkeys & KEY_CROUCH))
	{
		if(Player_MovingObjects[playerid] == 2)
	    {
	        new Float:rPX, Float:rPY, Float:rPZ, Float:rRX, Float:rRY, Float:rRZ;
		    AttachObjectToObjectEx(noclipdata[playerid][flyobject], CamOffSetX, CamOffSetY, CamOffSetZ, 0.0, 0.0, 0.0, rPX, rPY, rPZ, rRX, rRY, rRZ, playerid);
			new ResolveObject = CreateObject(0, rPX, rPY, rPZ, rRX, rRY, rRZ);
	        for(new i = 0; i != MAX_MAPMOVER_OBJECTS; i++)
            {
                if(MAPMOVER_OBJECTS[i][ModelID] != 0)
                {
				    new Float:PX, Float:PY, Float:PZ;
				    new Float:RX, Float:RY, Float:RZ;
				    AttachObjectToObjectEx(ResolveObject, MAPMOVER_OBJECTS[i][OffSetX], MAPMOVER_OBJECTS[i][OffSetY], MAPMOVER_OBJECTS[i][OffSetZ], MAPMOVER_OBJECTS[i][RotationX], MAPMOVER_OBJECTS[i][RotationY], MAPMOVER_OBJECTS[i][RotationZ], PX, PY, PZ, RX, RY, RZ);

					MAPMOVER_OBJECTS[i][OffSetX] = 0.0;
                    MAPMOVER_OBJECTS[i][OffSetY] = 0.0;
                    MAPMOVER_OBJECTS[i][OffSetZ] = 0.0;

                    DestroyObject(MAPMOVER_OBJECTS[i][ObjectID]);
                    MAPMOVER_OBJECTS[i][ObjectID] = CreateObject(MAPMOVER_OBJECTS[i][ModelID], PX, PY, PZ, RX, RY, RZ);
                    MAPMOVER_OBJECTS[i][PositionX] = PX;
                    MAPMOVER_OBJECTS[i][PositionY] = PY;
                    MAPMOVER_OBJECTS[i][PositionZ] = PZ;
                    MAPMOVER_OBJECTS[i][RotationX] = RX;
                    MAPMOVER_OBJECTS[i][RotationY] = RY;
                    MAPMOVER_OBJECTS[i][RotationZ] = RZ;
				}
            }
            DestroyObject(ResolveObject);
            DestroyObject(ObjectCenter);
            ObjectCenter = CreateObject(1220, rPX, rPY, rPZ, 0.0, 0.0, 0.0);
            Player_MovingObjects[playerid] = 0;
	        return 1;
	    }
	}
	return 1;
}

public OnPlayerEditObject(playerid, playerobject, objectid, response, Float:fX, Float:fY, Float:fZ, Float:fRotX, Float:fRotY, Float:fRotZ)
{
    if(response == EDIT_RESPONSE_FINAL || response == EDIT_RESPONSE_CANCEL)
	{
		if(objectid == ObjectCenter)
		{
  			SetObjectPos(objectid, fX, fY, fZ);
   			SetObjectRot(objectid, fRotX, fRotY, fRotZ);
		    if(Player_MovingObjects[playerid] == 1)
		    {
		        for(new i = 0; i != MAX_MAPMOVER_OBJECTS; i++)
                {
                    if(MAPMOVER_OBJECTS[i][ModelID] != 0)
                    {
					    new Float:PX, Float:PY, Float:PZ;
					    new Float:RX, Float:RY, Float:RZ;
					    AttachObjectToObjectEx(ObjectCenter, MAPMOVER_OBJECTS[i][OffSetX], MAPMOVER_OBJECTS[i][OffSetY], MAPMOVER_OBJECTS[i][OffSetZ], MAPMOVER_OBJECTS[i][RotationX], MAPMOVER_OBJECTS[i][RotationY], MAPMOVER_OBJECTS[i][RotationZ], PX, PY, PZ, RX, RY, RZ);

						MAPMOVER_OBJECTS[i][OffSetX] = 0.0;
                        MAPMOVER_OBJECTS[i][OffSetY] = 0.0;
                        MAPMOVER_OBJECTS[i][OffSetZ] = 0.0;

                        DestroyObject(MAPMOVER_OBJECTS[i][ObjectID]);
                        MAPMOVER_OBJECTS[i][ObjectID] = CreateObject(MAPMOVER_OBJECTS[i][ModelID], PX, PY, PZ, RX, RY, RZ);
                        MAPMOVER_OBJECTS[i][PositionX] = PX;
                        MAPMOVER_OBJECTS[i][PositionY] = PY;
                        MAPMOVER_OBJECTS[i][PositionZ] = PZ;
                        MAPMOVER_OBJECTS[i][RotationX] = RX;
                        MAPMOVER_OBJECTS[i][RotationY] = RY;
                        MAPMOVER_OBJECTS[i][RotationZ] = RZ;
					}
                }
                SetObjectRot(objectid, 0.0, 0.0, 0.0);
                Player_MovingObjects[playerid] = 0;
		        return 1;
		    }
	        SetObjectMaterial(ObjectCenter, 0, 0, "null", "null");
	        SendClientMessage(playerid, -1, "{FFFF00}Map Mover: {FFFFFF}Use the {FFFF00}/map {FFFFFF}command again to show the main menu.");
	        SetObjectRot(objectid, 0.0, 0.0, 0.0);
		}
	}
	return 1;
}

AttachObjectToObjectEx(attachoid, Float:off_x, Float:off_y, Float:off_z, Float:rot_x, Float:rot_y, Float:rot_z, &Float:X, &Float:Y, &Float:Z, &Float:RX, &Float:RY, &Float:RZ, pobject = -1) // By Stylock - http://forum.sa-mp.com/member.php?u=114165
{
	static
		Float:sin[3],
		Float:cos[3],
		Float:pos[3],
		Float:rot[3];
	if(pobject == -1)
	{
		GetObjectPos(attachoid, pos[0], pos[1], pos[2]);
		GetObjectRot(attachoid, rot[0], rot[1], rot[2]);
	}
	else
	{
		GetPlayerObjectPos(pobject, attachoid, pos[0], pos[1], pos[2]);
		GetPlayerObjectRot(pobject, attachoid, rot[0], rot[1], rot[2]);
	}
	EDIT_FloatEulerFix(rot[0], rot[1], rot[2]);
	cos[0] = floatcos(rot[0], degrees); cos[1] = floatcos(rot[1], degrees); cos[2] = floatcos(rot[2], degrees); sin[0] = floatsin(rot[0], degrees); sin[1] = floatsin(rot[1], degrees); sin[2] = floatsin(rot[2], degrees);
	pos[0] = pos[0] + off_x * cos[1] * cos[2] - off_x * sin[0] * sin[1] * sin[2] - off_y * cos[0] * sin[2] + off_z * sin[1] * cos[2] + off_z * sin[0] * cos[1] * sin[2];
	pos[1] = pos[1] + off_x * cos[1] * sin[2] + off_x * sin[0] * sin[1] * cos[2] + off_y * cos[0] * cos[2] + off_z * sin[1] * sin[2] - off_z * sin[0] * cos[1] * cos[2];
	pos[2] = pos[2] - off_x * cos[0] * sin[1] + off_y * sin[0] + off_z * cos[0] * cos[1];
	rot[0] = asin(cos[0] * cos[1]); rot[1] = atan2(sin[0], cos[0] * sin[1]) + rot_z; rot[2] = atan2(cos[1] * cos[2] * sin[0] - sin[1] * sin[2], cos[2] * sin[1] - cos[1] * sin[0] * -sin[2]);
	cos[0] = floatcos(rot[0], degrees); cos[1] = floatcos(rot[1], degrees); cos[2] = floatcos(rot[2], degrees); sin[0] = floatsin(rot[0], degrees); sin[1] = floatsin(rot[1], degrees); sin[2] = floatsin(rot[2], degrees);
	rot[0] = asin(cos[0] * sin[1]); rot[1] = atan2(cos[0] * cos[1], sin[0]); rot[2] = atan2(cos[2] * sin[0] * sin[1] - cos[1] * sin[2], cos[1] * cos[2] + sin[0] * sin[1] * sin[2]);
	cos[0] = floatcos(rot[0], degrees); cos[1] = floatcos(rot[1], degrees); cos[2] = floatcos(rot[2], degrees); sin[0] = floatsin(rot[0], degrees); sin[1] = floatsin(rot[1], degrees); sin[2] = floatsin(rot[2], degrees);
	rot[0] = atan2(sin[0], cos[0] * cos[1]) + rot_x; rot[1] = asin(cos[0] * sin[1]); rot[2] = atan2(cos[2] * sin[0] * sin[1] + cos[1] * sin[2], cos[1] * cos[2] - sin[0] * sin[1] * sin[2]);
	cos[0] = floatcos(rot[0], degrees); cos[1] = floatcos(rot[1], degrees); cos[2] = floatcos(rot[2], degrees); sin[0] = floatsin(rot[0], degrees); sin[1] = floatsin(rot[1], degrees); sin[2] = floatsin(rot[2], degrees);
	rot[0] = asin(cos[1] * sin[0]); rot[1] = atan2(sin[1], cos[0] * cos[1]) + rot_y; rot[2] = atan2(cos[0] * sin[2] - cos[2] * sin[0] * sin[1], cos[0] * cos[2] + sin[0] * sin[1] * sin[2]);
	X = pos[0];
	Y = pos[1];
	Z = pos[2];
	RX = rot[0];
	RY = rot[1];
 	RZ = rot[2];
}


EDIT_FloatEulerFix(&Float:rot_x, &Float:rot_y, &Float:rot_z)
{
    EDIT_FloatGetRemainder(rot_x, rot_y, rot_z);
    if((!floatcmp(rot_x, 0.0) || !floatcmp(rot_x, 360.0))
    && (!floatcmp(rot_y, 0.0) || !floatcmp(rot_y, 360.0)))
    {
        rot_y = 0.0000002;
    }
    return 1;
}

EDIT_FloatGetRemainder(&Float:rot_x, &Float:rot_y, &Float:rot_z)
{
    EDIT_FloatRemainder(rot_x, 360.0);
    EDIT_FloatRemainder(rot_y, 360.0);
    EDIT_FloatRemainder(rot_z, 360.0);
    return 1;
}

EDIT_FloatRemainder(&Float:remainder, Float:value)
{
    if(remainder >= value)
    {
        while(remainder >= value)
        {
            remainder = remainder - value;
        }
    }
    else if(remainder < 0.0)
    {
        while(remainder < 0.0)
        {
            remainder = remainder + value;
        }
    }
    return 1;
}
//--------------------------------------------------

public OnPlayerUpdate(playerid)
{
	if(noclipdata[playerid][cameramode] == CAMERA_MODE_FLY)
	{
		new keys,ud,lr;
		GetPlayerKeys(playerid,keys,ud,lr);
		if(noclipdata[playerid][mode] && (GetTickCount() - noclipdata[playerid][lastmove] > 100))
		{
		    // If the last move was > 100ms ago, process moving the object the players camera is attached to
		    MoveCamera(playerid);
		}

		// Is the players current key state different than their last keystate?
		if(noclipdata[playerid][udold] != ud || noclipdata[playerid][lrold] != lr)
		{
			if((noclipdata[playerid][udold] != 0 || noclipdata[playerid][lrold] != 0) && ud == 0 && lr == 0)
			{   // All keys have been released, stop the object the camera is attached to and reset the acceleration multiplier
				StopPlayerObject(playerid, noclipdata[playerid][flyobject]);
				noclipdata[playerid][mode]      = 0;
				noclipdata[playerid][accelmul]  = 0.0;
			}
			else
			{   // Indicates a new key has been pressed

			    // Get the direction the player wants to move as indicated by the keys
				noclipdata[playerid][mode] = GetMoveDirectionFromKeys(ud, lr);

				// Process moving the object the players camera is attached to
				MoveCamera(playerid);
			}
		}
		noclipdata[playerid][udold] = ud; noclipdata[playerid][lrold] = lr; // Store current keys pressed for comparison next update
		return 0;
	}
	return 1;
}

//--------------------------------------------------

stock GetMoveDirectionFromKeys(ud, lr)
{
	new direction = 0;

    if(lr < 0)
	{
		if(ud < 0) 		direction = MOVE_FORWARD_LEFT; 	// Up & Left key pressed
		else if(ud > 0) direction = MOVE_BACK_LEFT; 	// Back & Left key pressed
		else            direction = MOVE_LEFT;          // Left key pressed
	}
	else if(lr > 0) 	// Right pressed
	{
		if(ud < 0)      direction = MOVE_FORWARD_RIGHT;  // Up & Right key pressed
		else if(ud > 0) direction = MOVE_BACK_RIGHT;     // Back & Right key pressed
		else			direction = MOVE_RIGHT;          // Right key pressed
	}
	else if(ud < 0) 	direction = MOVE_FORWARD; 	// Up key pressed
	else if(ud > 0) 	direction = MOVE_BACK;		// Down key pressed

	return direction;
}

//--------------------------------------------------

stock MoveCamera(playerid)
{
	new Float:FV[3], Float:CP[3];
	GetPlayerCameraPos(playerid, CP[0], CP[1], CP[2]);          // 	Cameras position in space
    GetPlayerCameraFrontVector(playerid, FV[0], FV[1], FV[2]);  //  Where the camera is looking at

	// Increases the acceleration multiplier the longer the key is held
	if(noclipdata[playerid][accelmul] <= 1) noclipdata[playerid][accelmul] += ACCEL_RATE;

	// Determine the speed to move the camera based on the acceleration multiplier
	new Float:speed = MOVE_SPEED * noclipdata[playerid][accelmul];

	// Calculate the cameras next position based on their current position and the direction their camera is facing
	new Float:X, Float:Y, Float:Z;
	GetNextCameraPosition(noclipdata[playerid][mode], CP, FV, X, Y, Z);
	MovePlayerObject(playerid, noclipdata[playerid][flyobject], X, Y, Z, speed);

	// Store the last time the camera was moved as now
	noclipdata[playerid][lastmove] = GetTickCount();
	return 1;
}

//--------------------------------------------------

stock GetNextCameraPosition(move_mode, Float:CP[3], Float:FV[3], &Float:X, &Float:Y, &Float:Z)
{
    // Calculate the cameras next position based on their current position and the direction their camera is facing
    #define OFFSET_X (FV[0]*6000.0)
	#define OFFSET_Y (FV[1]*6000.0)
	#define OFFSET_Z (FV[2]*6000.0)
	switch(move_mode)
	{
		case MOVE_FORWARD:
		{
			X = CP[0]+OFFSET_X;
			Y = CP[1]+OFFSET_Y;
			Z = CP[2]+OFFSET_Z;
		}
		case MOVE_BACK:
		{
			X = CP[0]-OFFSET_X;
			Y = CP[1]-OFFSET_Y;
			Z = CP[2]-OFFSET_Z;
		}
		case MOVE_LEFT:
		{
			X = CP[0]-OFFSET_Y;
			Y = CP[1]+OFFSET_X;
			Z = CP[2];
		}
		case MOVE_RIGHT:
		{
			X = CP[0]+OFFSET_Y;
			Y = CP[1]-OFFSET_X;
			Z = CP[2];
		}
		case MOVE_BACK_LEFT:
		{
			X = CP[0]+(-OFFSET_X - OFFSET_Y);
 			Y = CP[1]+(-OFFSET_Y + OFFSET_X);
		 	Z = CP[2]-OFFSET_Z;
		}
		case MOVE_BACK_RIGHT:
		{
			X = CP[0]+(-OFFSET_X + OFFSET_Y);
 			Y = CP[1]+(-OFFSET_Y - OFFSET_X);
		 	Z = CP[2]-OFFSET_Z;
		}
		case MOVE_FORWARD_LEFT:
		{
			X = CP[0]+(OFFSET_X  - OFFSET_Y);
			Y = CP[1]+(OFFSET_Y  + OFFSET_X);
			Z = CP[2]+OFFSET_Z;
		}
		case MOVE_FORWARD_RIGHT:
		{
			X = CP[0]+(OFFSET_X  + OFFSET_Y);
			Y = CP[1]+(OFFSET_Y  - OFFSET_X);
			Z = CP[2]+OFFSET_Z;
		}
	}
}
//--------------------------------------------------

stock CancelFlyMode(playerid)
{
	DeletePVar(playerid, "FlyMode");
	CancelEdit(playerid);
	TogglePlayerSpectating(playerid, false);

	DestroyPlayerObject(playerid, noclipdata[playerid][flyobject]);
	noclipdata[playerid][cameramode] = CAMERA_MODE_NONE;
	return 1;
}

//--------------------------------------------------

stock FlyMode(playerid)
{
	// Create an invisible object for the players camera to be attached to
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	noclipdata[playerid][flyobject] = CreatePlayerObject(playerid, 19300, X, Y, Z, 0.0, 0.0, 0.0);

	// Place the player in spectating mode so objects will be streamed based on camera location
	TogglePlayerSpectating(playerid, true);
	// Attach the players camera to the created object
	AttachCameraToPlayerObject(playerid, noclipdata[playerid][flyobject]);

	SetPVarInt(playerid, "FlyMode", 1);
	noclipdata[playerid][cameramode] = CAMERA_MODE_FLY;
	return 1;
}

//--------------------------------------------------