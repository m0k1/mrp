#include <a_samp>
#include <streamer>
#include <RouteConnector>
#include <YSI\y_commands>
#include <YSI\y_master>
#include <sscanf2>

new icon = 56;
new bool:PlayerOnRoute[MAX_PLAYERS] = {false,...};

enum Data
{
	RouteID,
	Icons[1024],
	IconsSize,
	LostCount,
	Calculating,
	Destination
}

#define STRAIGHT 1
#define LEFT_LOW 2
#define LEFT_HIGH 3
#define RIGHT_LOW 4
#define RIGHT_HIGH 5
#define UTURN_LEFT 6
#define UTURN_RIGHT 7


new Storage[MAX_PLAYERS][Data];

public GPS_WhenRouteIsCalculated(routeid,node_id_array[],amount_of_nodes,Float:distance,Float:Polygon[],Polygon_Size)
{

	if(amount_of_nodes > 1)
	{
	DestroyDynamicArea(Storage[routeid][RouteID]);
	Storage[routeid][RouteID] = CreateDynamicPolygon(Polygon,.maxpoints=Polygon_Size,.playerid=routeid);
	new Counter=0;
	for(new i = 0; i < Storage[routeid][IconsSize]; ++i)
	{
		DestroyDynamicMapIcon(Storage[routeid][Icons][i]);
	}
	for(new i = 0,j=(Polygon_Size-2)/2; i < j; i+=8)
	{
		Storage[routeid][Icons][Counter++] = CreateDynamicMapIcon(Polygon[i], Polygon[i+1], 0.0, icon, 0x0000FFFF, -1, -1, routeid, 300.0);

	}
	Storage[routeid][Destination] = node_id_array[amount_of_nodes-1];
	Storage[routeid][IconsSize] = Counter;
	Storage[routeid][Calculating] = 0;
	Storage[routeid][LostCount] = 0;
	}
	else
	{
	DestroyDynamicArea(Storage[routeid][RouteID]);
	Storage[routeid][RouteID] = (-1);
	for(new i = 0; i < Storage[routeid][IconsSize]; ++i)
	{
		DestroyDynamicMapIcon(Storage[routeid][Icons][i]);
	}
	Storage[routeid][Destination] = (-1);
	Storage[routeid][IconsSize] = 0;
	Storage[routeid][Calculating] = 0;
	Storage[routeid][LostCount] = 0;
	}
	return 1;
}


public OnPlayerEnterDynamicArea(playerid,areaid)
{
	if(Storage[playerid][RouteID] == areaid)
	{
	Storage[playerid][LostCount] = 0;
	PlayerOnRoute[playerid] = true;
	PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/RouteIn.mp3");
	SendClientMessage(playerid,0x00FF00FF,"Usao si u GPS rutu :)");
	}

	return 1;
}

public OnPlayerLeaveDynamicArea(playerid,areaid)
{
	if(Storage[playerid][RouteID] == areaid)
	{
	PlayerOnRoute[playerid] = false;
	SendClientMessage(playerid,0x00FF00FF,"Izasao si iz gps rute");
	PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/RouteOut.mp3");

	if(Storage[playerid][Calculating] == 0)
	{
		CalculatePath(NearestPlayerNode(playerid),Storage[playerid][Destination],playerid,true,12.0);
		Storage[playerid][Calculating] = 1;
	}
	}
	return 1;
}

public OnPlayerConnect(playerid)
{
	gps_AddPlayer(playerid);
	Storage[playerid][RouteID] = (-1);
	Storage[playerid][IconsSize] = 0;
	Storage[playerid][LostCount] = 0;
	Storage[playerid][Calculating] = 0;
	Storage[playerid][Destination] = (-1);
	return 1;
}

public OnPlayerDisconnect(playerid,reason)
{
	gps_RemovePlayer(playerid);
	if(Storage[playerid][RouteID] != (-1))
	{
	DestroyDynamicArea(Storage[playerid][RouteID]);
	Storage[playerid][RouteID] = (-1);
	}
	if(Storage[playerid][IconsSize] != 0)
	{
	for(new i = 0; i < Storage[playerid][IconsSize]; ++i)
	{
		DestroyDynamicMapIcon(Storage[playerid][Icons][i]);
	}
	Storage[playerid][IconsSize] = 0;
	}
	return 1;
}

public OnPlayerClosestNodeIDChange(playerid,old_NodeID,new_NodeID)
{
	if(Storage[playerid][RouteID] != (-1))
	{
	if(Storage[playerid][Destination] != (-1))
	{
		if(old_NodeID == Storage[playerid][Destination] || new_NodeID == Storage[playerid][Destination])
		{
		
		PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/RouteEnd.mp3");
		DestroyDynamicArea(Storage[playerid][RouteID]);
		for(new i = 0; i < Storage[playerid][IconsSize]; ++i)
		{
			DestroyDynamicMapIcon(Storage[playerid][Icons][i]);
		}
		Storage[playerid][RouteID] = (-1);
		Storage[playerid][IconsSize] = 0;
		Storage[playerid][LostCount] = 0;
		Storage[playerid][Calculating] = 0;
		Storage[playerid][Destination] = (-1);
		}
		else
		{
		/*for(new i = 5,j = -4; i < j; ++i)
		{
			if(GetRouteAtPos(Storage[playerid][RouteID],i-5) == new_NodeID)
			{

			}
		}
		*/
			//	new result = IsLeftStraighRight(GetRouteAtPos(Storage[playerid][RouteID],i+2),GetRouteAtPos(Storage[playerid][RouteID],i+3),GetRouteAtPos(Storage[playerid][RouteID],i+4));
	//	printf("skreni %s", GetRouteArraySize(Storage[playerid][RouteID]));
		if(!IsPlayerInDynamicArea(playerid,Storage[playerid][RouteID]))
		{
			if(Storage[playerid][Calculating] == 0)
			{
			if(++Storage[playerid][LostCount] >= 4)
			{
				Storage[playerid][Calculating] = 1;
				CalculatePath(NearestPlayerNode(playerid),Storage[playerid][Destination],playerid,true,9.5);
			}
			}
		}
		}
	}
	else
	{
		DestroyDynamicArea(Storage[playerid][RouteID]);
		for(new i = 0; i < Storage[playerid][IconsSize]; ++i)
		{
		DestroyDynamicMapIcon(Storage[playerid][Icons][i]);
		}
		Storage[playerid][RouteID] = (-1);
		Storage[playerid][IconsSize] = 0;
		Storage[playerid][LostCount] = 0;
		Storage[playerid][Calculating] = 0;
		Storage[playerid][Destination] = (-1);
	}
	}
	return 1;
}

public OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ)
{
	if(Storage[playerid][Calculating] == 0)
	{
	if(Storage[playerid][Destination] != (-1) || Storage[playerid][RouteID] !=(-1))
	{
		DestroyDynamicArea(Storage[playerid][RouteID]);
		for(new i = 0; i < Storage[playerid][IconsSize]; ++i)
		{
		DestroyDynamicMapIcon(Storage[playerid][Icons][i]);
		}
		Storage[playerid][RouteID] = (-1);
		Storage[playerid][IconsSize] = 0;
		Storage[playerid][LostCount] = 0;
		Storage[playerid][Calculating] = 0;
		Storage[playerid][Destination] = (-1);
	}
	CalculatePath(NearestPlayerNode(playerid),NearestNodeFromPoint(fX,fY,fZ),playerid,true,12.0);
	Storage[playerid][Calculating] = 1;
	}
	return 1;
}

stock IsLeftStraighRight(NodeFrom,NodeMiddle,NodeTo,&Float:refAngle=0.0)
{
	new Float:DeltaX;
	new Float:DeltaY;
	new Float:AdderX;
	new Float:AdderY;
	new Float:Length;
	new Float:CP[9][2];
	GetNodePos(NodeFrom,CP[1][0],CP[1][1],DeltaX);
	GetNodePos(NodeMiddle,CP[0][0],CP[0][1],DeltaX);
	GetNodePos(NodeTo,CP[2][0],CP[2][1],DeltaX);
	
	DeltaX = CP[1][0] - CP[0][0];
	DeltaY = CP[1][1] - CP[0][1];
	Length = floatsqroot(((DeltaX * DeltaX) + (DeltaY * DeltaY))) + 0.0000000001;
	AdderX = (DeltaX / Length);
	AdderY = (DeltaY / Length);
	CP[3][0] = (CP[0][0] + (AdderX * 30.0));
	CP[3][1] = (CP[0][1] + (AdderY * 30.0));

	DeltaX = CP[2][0] - CP[0][0];
	DeltaY = CP[2][1] - CP[0][1];
	Length = floatsqroot(((DeltaX * DeltaX) + (DeltaY * DeltaY))) + 0.0000000001;
	AdderX = (DeltaX / Length);
	AdderY = (DeltaY / Length);
	CP[4][0] = (CP[0][0] + (AdderX * 30.0));
	CP[4][1] = (CP[0][1] + (AdderY * 30.0));

	DeltaX = CP[3][0] - CP[4][0];
	DeltaY = CP[3][1] - CP[4][1];
	Length = floatsqroot(((DeltaX * DeltaX) + (DeltaY * DeltaY))) + 0.0000000001;
	AdderX = (DeltaX / Length);
	AdderY = (DeltaY / Length);
	CP[8][0] = (CP[4][0] + (AdderX * Length / 2.0));
	CP[8][1] = (CP[4][1] + (AdderY * Length / 2.0));

	DeltaX = CP[8][0] - CP[0][0];
	DeltaY = CP[8][1] - CP[0][1];
	Length = floatsqroot(((DeltaX * DeltaX) + (DeltaY * DeltaY))) + 0.0000000001;
	AdderX = (DeltaX / Length);
	AdderY = (DeltaY / Length);
	CP[7][0] = (CP[0][0] - (AdderX * 30.0));
	CP[7][1] = (CP[0][1] - (AdderY * 30.0));

	new Float:result = (CP[2][0] - CP[1][0]) * (CP[7][1] - CP[1][1]) - (CP[2][1] - CP[1][1]) * (CP[7][0] - CP[1][0]);
	new Float:Angle = floatabs(GetAngleBetweenNodes(NodeFrom,NodeMiddle,NodeTo));
	refAngle = Angle;
	if(result < 0.0)
	{
	if(Angle <= 75.0)
	{
		return UTURN_LEFT;
	}
	else if(Angle <= 135.0)
	{
		return LEFT_HIGH;
	}
	else if(Angle <= 177.5)
	{
		return LEFT_LOW;
	}
	else
	{
		return STRAIGHT;
	}
	}
	else if(result == 0.0)
	{
	return STRAIGHT;
	}
	else if(result > 0.0)
	{
	if(Angle <= 75.0)
	{
		return UTURN_RIGHT;
	}
	else if(Angle <= 135.0)
	{
		return RIGHT_HIGH;
	}
	else if(Angle <= 177.5)
	{
		return RIGHT_LOW;
	}
	else
	{
		return STRAIGHT;
	}
	}
	return STRAIGHT;
}

YCMD:gpsremoveme(playerid, params[], help)
{
	gps_RemovePlayer(playerid);
	SendClientMessage(playerid, -1,"izbrisan");
	return 1;
}

YCMD:gpsaddme(playerid, params[], help)
{
	gps_AddPlayer(playerid);
	SendClientMessage(playerid, -1,"dodat");
	return 1;
}

YCMD:testt(playerid, params[], help)
{
	new result = GetRouteAtPos(Storage[playerid][RouteID],133);
	new string[100];
	result = result + 2;
	format(string,sizeof(string), "DEBUG: %i ===", result);
	SendClientMessage(playerid, -1,string);
	return 1;
}

YCMD:gpsldp(playerid, params[], help)
{
	new Lala[3];
	if(sscanf(params, "iii",Lala[0],Lala[1],Lala[2])) return SendClientMessage(playerid, -1, "Format: /gpsldp NodeFrom,NodeMiddle,NodeTo !");
	new result = IsLeftStraighRight(Lala[0],Lala[1],Lala[2]);
	switch(result)
	{
		case STRAIGHT:
		{
			SendClientMessage(playerid,-1,"pravo");
			PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/Follow.mp3");
		}
		case LEFT_LOW:
		{
			SendClientMessage(playerid,-1,"<<<<<<<<<<<<<<<<");
			PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/Left.mp3");
		}
		case LEFT_HIGH:
		{
			SendClientMessage(playerid,-1,"<<<<<<<<<<<<<<<<H");
			PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/Left.mp3");
		}
		case RIGHT_LOW:
		{
			SendClientMessage(playerid,-1,">>>>>>>>>>>>>>>>");
			PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/Right.mp3");
		}
		case RIGHT_HIGH:
		{
			SendClientMessage(playerid,-1,"H>>>>>>>>>>>>>>>>");
			PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/Right.mp3");
		}
		case UTURN_LEFT:
		{
			SendClientMessage(playerid,-1,"<<<<<<<<<<<<<<<<<U");
			PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/UTurnLeft.mp3");
		}
		case UTURN_RIGHT:
		{
			SendClientMessage(playerid,-1,">>>>>>>>>>>>>>>>>U");
			PlayAudioStreamForPlayer(playerid,"http://smhq.cf/gps/UTurnRight.mp3");
		}
	}
	return 1;
}