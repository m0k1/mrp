#define FILTERSCRIPT
#include <a_samp>
#include <streamer>
#include <RouteConnector>

new SpdObj[MAX_PLAYERS][5];
new bool:UpdateSpeed[MAX_PLAYERS] = {false,...};

public OnPlayerConnect(playerid)
{
	UpdateSpeed[playerid] = false;
	SpdObj[playerid][0] = INVALID_OBJECT_ID;
	SpdObj[playerid][1] = INVALID_OBJECT_ID;
	SpdObj[playerid][2] = INVALID_OBJECT_ID;
	SpdObj[playerid][3] = INVALID_OBJECT_ID;
	SpdObj[playerid][4] = INVALID_OBJECT_ID;
	return 0;
}
 
public OnPlayerDisconnect(playerid,reason)
{
	#pragma unused reason
	if(SpdObj[playerid][0] != INVALID_OBJECT_ID)
	{
		DestroyDynamicObject(SpdObj[playerid][0]);
		DestroyDynamicObject(SpdObj[playerid][1]);
		DestroyDynamicObject(SpdObj[playerid][2]);
		DestroyDynamicObject(SpdObj[playerid][3]);
		DestroyDynamicObject(SpdObj[playerid][4]);
	}
	return 0;
}
 
public OnPlayerUpdate(playerid)
{
	if(UpdateSpeed[playerid])
	{
		new Float:p[3];
		GetVehicleVelocity(GetPlayerVehicleID(playerid),p[0],p[1],p[2]);
		new str[12];
		format(str,12,"%.0f KM/H",150.0*(p[0]*p[0]+p[1]*p[1]));
		SetDynamicObjectMaterialText(SpdObj[playerid][0],0,str,OBJECT_MATERIAL_SIZE_512x256,"Arial",43,true,0xFFFFFFFF,0,OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
	}
	return 1;
}
 
public OnPlayerStateChange(playerid,newstate,oldstate)
{
	if(newstate == PLAYER_STATE_DRIVER)
	{
		SpdObj[playerid][0] = CreateDynamicObject(19482,0.0,0.0,0.0,0.0,0.0,0.0,-1,-1,playerid,200.0);
		SpdObj[playerid][1] = CreateDynamicObject(19482,0.0,0.0,0.0,0.0,0.0,0.0,-1,-1,playerid,200.0);
		SpdObj[playerid][2] = CreateDynamicObject(19482,0.0,0.0,0.0,0.0,0.0,0.0,-1,-1,playerid,200.0);
		SpdObj[playerid][3] = CreateDynamicObject(19482,0.0,0.0,0.0,0.0,0.0,0.0,-1,-1,playerid,200.0);
		SpdObj[playerid][4] = CreateDynamicObject(19482,0.0,0.0,0.0,0.0,0.0,0.0,-1,-1,playerid,200.0);
		new Float:x,Float:y,Float:z;
		GetVehicleModelInfo(GetVehicleModel(GetPlayerVehicleID(playerid)),VEHICLE_MODEL_INFO_SIZE,x,y,z);
		//----------------------------------------------brzina
		AttachDynamicObjectToVehicle(SpdObj[playerid][0],GetPlayerVehicleID(playerid),-x+0.2,0.0,z/2-0.3,0.0,0.0,270.0);
		//----------------------------------------------linija dole
		SetDynamicObjectMaterialText(SpdObj[playerid][1],0,"_________",OBJECT_MATERIAL_SIZE_512x256,"Arial",40,true,0xFFf53302 ,0,OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
		AttachDynamicObjectToVehicle(SpdObj[playerid][1],GetPlayerVehicleID(playerid),-x+0.2,0.0,z/2-0.3,0.0,0.0,270.0);
		//----------------------------------------------- KOLICINA GORIVA
		SetDynamicObjectMaterialText(SpdObj[playerid][4],0,"100 L",OBJECT_MATERIAL_SIZE_512x256,"Arial",35,true,0xFFFFFFFF,0,OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
		AttachDynamicObjectToVehicle(SpdObj[playerid][4],GetPlayerVehicleID(playerid),-x+0.2,0.0,z/2-0.71,0.0,0.0,270.0);
		//-------------------------------------------------------------------------------
		SetDynamicObjectMaterialText(SpdObj[playerid][2],0,"L",OBJECT_MATERIAL_SIZE_512x256,"Arial",70,true,0xFFf53302 ,0,OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
		AttachDynamicObjectToVehicle(SpdObj[playerid][2],GetPlayerVehicleID(playerid),-x+0.2,0.0,z/2+0.2,180, 0, -90);
		//---------------------------------------------- GPS STRELICA (glava)
		SetDynamicObjectMaterialText(SpdObj[playerid][3],0,"<",OBJECT_MATERIAL_SIZE_512x256,"Arial",50,true,0xFFf53302 ,0,OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
		AttachDynamicObjectToVehicle(SpdObj[playerid][3],GetPlayerVehicleID(playerid),-x+0.1,0.0,z/2+0.39,180, 0, 90);

		Streamer_Update(playerid);
		UpdateSpeed[playerid] = true;
		return 1;
	}
	if(oldstate == PLAYER_STATE_DRIVER)
	{
		UpdateSpeed[playerid] = false;
		DestroyDynamicObject(SpdObj[playerid][0]);
		DestroyDynamicObject(SpdObj[playerid][1]);
		DestroyDynamicObject(SpdObj[playerid][2]);
		DestroyDynamicObject(SpdObj[playerid][3]);
		DestroyDynamicObject(SpdObj[playerid][4]);
		SpdObj[playerid][0] = INVALID_OBJECT_ID;
		SpdObj[playerid][1] = INVALID_OBJECT_ID;
		SpdObj[playerid][2] = INVALID_OBJECT_ID;
		SpdObj[playerid][3] = INVALID_OBJECT_ID;
		SpdObj[playerid][4] = INVALID_OBJECT_ID;
		return 1;
	}
	return 0;
}

stock gps_strelica(pravac,postavi,playerid)
{
	if(pravac == "levo")
	{


	}
	if(pravac == "desno")
	{
		/* STRELICA NA DESNO
		SetDynamicObjectMaterialText(SpdObj[playerid][2],0,"L",OBJECT_MATERIAL_SIZE_512x256,"Arial",70,true,0xFFf53302 ,0,OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
		AttachDynamicObjectToVehicle(SpdObj[playerid][2],GetPlayerVehicleID(playerid),-x+0.2,0.0,z/2+0.2,180, 0, 90);
		//---------------------------------------------- GPS STRELICA (glava)
	
		SetDynamicObjectMaterialText(SpdObj[playerid][3],0,">",OBJECT_MATERIAL_SIZE_512x256,"Arial",50,true,0xFFf53302 ,0,OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
		AttachDynamicObjectToVehicle(SpdObj[playerid][3],GetPlayerVehicleID(playerid),-x+0.3,0.0,z/2+0.4,180, 0, 90);
*/
	}
	if(pravac == "pravo")
	{
				//---------------------------------------------- GPS STRELICA (telo)
		SetDynamicObjectMaterialText(SpdObj[playerid][2],0,"I",OBJECT_MATERIAL_SIZE_512x256,"Arial",70,true,0xFFf53302 ,0,OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
		AttachDynamicObjectToVehicle(SpdObj[playerid][2],GetPlayerVehicleID(playerid),-x+0.2,0.0,z/2+0.2,180, 0, -90);
		//---------------------------------------------- GPS STRELICA (glava)
		//	native SetDynamicObjectMaterialText(objectid, materialindex, const text[], materialsize = OBJECT_MATERIAL_SIZE_256x128, const fontface[] = "Arial", fontsize = 24, bold = 1, fontcolor = 0xFFFFFFFF, backcolor = 0, textalignment = 0);
		// native SetObjectMaterialText(objectid, text[], materialindex = 0, materialsize = OBJECT_MATERIAL_SIZE_256x128, fontface[] = "Arial", fontsize = 24, bold = 1, fontcolor = 0xFFFFFFFF, backcolor = 0, textalignment = 0)
		SetDynamicObjectMaterialText(SpdObj[playerid][3],0,"V",OBJECT_MATERIAL_SIZE_512x256,"Arial",50,true,0xFFf53302 ,0,OBJECT_MATERIAL_TEXT_ALIGN_CENTER);
		AttachDynamicObjectToVehicle(SpdObj[playerid][3],GetPlayerVehicleID(playerid),-x+0.22,0.0,z/2+0.35,180, 0, 90);
	}
}



