
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
***		GPS Debug Tool v1 - by NaS - for RouteConnector Plugin by Gamer_Z - Thanks to him.                                            ***
*                                                                                                                                       *
*                                                                                                                                       *
*		This is a little tool to show ALL nodes and connections ingame, using 3D Texts                                                  *
*                                                                                                                                       *
*       It also creates a MapIcon at Intersections (nodes with at least 3 connections to other nodes).                                  *
*                                                                                                                                       *
*		It's a nice debug tool to view the nodes ingame and to see, if they are connected correctly.                                    *
*                                                                                                                                       *
*                                                                                                                                       *
*       To turn the labels off, type /gpsdoff.                                                                                         *
*       To turn them on again, type /gpsdon.                                                                                           *
*                                                                                                                                       *
*       This will change your virtual world, so you can't see other players anymore.                                                    *
*       The labels are only visible in world 0.                                                                                         *
*                                                                                                                                       *
*       To change the draw distance (because of client performance problems) change NODE_DRAW_DISTANCE and CONNECTION_DRAW_DISTANCE.    *
*       CONNECTION_DRAW_DISTANCE should be a bit lower as NODE_DRAW_DISTANCE.                                                           *
*                                                                                                                                       *
***		You can change anything in this scipt as you like, but don't claim it as your own!                                            ***
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define NOOOOO

#include <a_samp>
#if defined NOOOOO
#include "streamer"
#endif
#include "RouteConnector"

//#define MAX_DOTS 500

//new gps_destination[MAX_PLAYERS];
//new Routes[MAX_PLAYERS][MAX_DOTS];


stock Float:GDBP(Float:X, Float:Y, Float:Z, Float: PointX, Float: PointY, Float: PointZ) return floatsqroot(floatadd(floatadd(floatpower(floatsub(X, PointX), 2.0), floatpower(floatsub(Y, PointY), 2.0)), floatpower(floatsub(Z, PointZ), 2.0)));
#if defined NOOOOO
#define MAP_ICON_DRAW_DISTANCE 500.0

#define NODE_DRAW_DISTANCE 45.0
#define CONNECTION_DRAW_DISTANCE 15.0

#define CONNECTION_COLOR 0xFF0000FF
// There is no node color here. Change it in the code below.
#endif
main() { }
new selected[MAX_PLAYERS][4];

public OnGameModeInit()
{
	#if defined NOOOOO
	SetTimer("Create",50,0);
	#endif
	return 1;
}
#if defined NOOOOO
forward Create();
public Create()
{
    print("Creating 3D Text Labels.");
    for(new i; i < MAX_NODES; i++) // Going through all nodes.
	{
	    new Float:X,Float:Y,Float:Z;
	    if(GetNodePos(i,X,Y,Z) == 1)
	    {
	        if(X == 0.0 && Y == 0.0 && Z == 0.0) continue;
			new str[168];
			new connectstr[168];
			new connections = GetConnectedNodes(i);
			
			if(connections == 0)
			{
			    printf("Found node without connections: %d",i);
			    new id = CreateDynamicMapIcon(X,Y,Z,53,0,_,_,_,7000);
			    Streamer_SetIntData(STREAMER_TYPE_MAP_ICON, id, E_STREAMER_STYLE, MAPICON_GLOBAL);
			}
			
			if(connections >= 3)
			{
			    CreateDynamicMapIcon(X,Y,Z,56,0,0,_,_,MAP_ICON_DRAW_DISTANCE);
			}
			
			format(connectstr,sizeof(connectstr),"Konekcije: {CCCCCC}%d",connections);
			if(connections > 0)
			{
			    format(connectstr,sizeof(connectstr),"%s\n{FFFFFF}Povezano sa:{CCCCCC}",connectstr);
			    for(new c; c < connections; c++)
			    {
			        new id = GetConnectedNodeID(i,c);
			        new ct = GetNodeDirectionToConnect(i,c);
			        if(ct == 0)
			        {
			        	format(connectstr,sizeof(connectstr),"%s %d(OBA)",connectstr,id);
					}
					else if(ct == 1)
					{
			        	format(connectstr,sizeof(connectstr),"%s %d(DOVDE)",connectstr,id);
					}
					else if(ct == 2)
					{
			        	format(connectstr,sizeof(connectstr),"%s %d(ODAVDE)",connectstr,id);
					}
					new Float:x, Float:y,Float:z;

					new Float:X2,Float:Y2,Float:Z2;
			        if(GetNodePos(id,X2,Y2,Z2) == 1)
			        {
				        for(new l=1; l <= 2; l++) // This goes through all connections of all nodes.
				        {
							//  Every connection is done TWICE. So, only create 1/2 connection route for every node (1/4 per Label):
							
					        x = X + (((X2 - X) / 5)*l);  // Setting off X's position by "percentage".
							y = Y + (((Y2 - Y) / 5)*l);  // Setting off Y's position by "percentage".
							z = Z + (((Z2 - Z) / 5)*l);  // Same for Z
							CreateDynamic3DTextLabel("<>",CONNECTION_COLOR,x,y,z+1,CONNECTION_DRAW_DISTANCE,_,_,0,0,_,_,CONNECTION_DRAW_DISTANCE+20.0); // I advise to not change the text to "HERE IS A CONNECTION" or somehing.
						}
					}

			    }
			}
			format(str,sizeof(str),"{FFFFFF}[{FFFF00}NODE{FFFFFF}]\nID: {CCCCCC}%d\n{FFFFFF}%s",i,connectstr);
		    CreateDynamic3DTextLabel(str,0xFFFFFFFF,X,Y,Z+1.2,NODE_DRAW_DISTANCE,_,_,0,0,_,_,NODE_DRAW_DISTANCE+20.0);
	    }
	}
	print("Done.");
}
#endif
//new counter[MAX_PLAYERS] = {0,...};

//public OnPlayerConnect(playerid)
//{
//    for(new x; x < MAX_DOTS; x++) Routes[playerid][x]=-1;
//	return 1;
//}

public GPS_WhenRouteIsCalculated(routeid,node_id_array[],amount_of_nodes,Float:distance,Float:Polygon[],Polygon_Size)
{
	for(new i = 0; i < amount_of_nodes; ++i)
	{
	    new Float:X,Float:Y,Float:Z,Float:A,Float:Ax,Float:Ay,Float:Az;
	    GetNodePos(node_id_array[i],X,Y,Z);
	    if(i < amount_of_nodes-3)
	    {
	        GetNodePos(node_id_array[i+3],Ax,Ay,Az);
	    	A =floatabs(270.0 - atan2( Ax-X,Ay-Y));
	    	CreateDynamicObject(1318,X,Y,Z+0.2,0.0,90.0,A);
	    }else
	    if(i < amount_of_nodes-2)
	    {
	        GetNodePos(node_id_array[i+2],Ax,Ay,Az);
	    	A =floatabs(270.0 - atan2( Ax-X,Ay-Y));
	    	CreateDynamicObject(1318,X,Y,Z+0.2,0.0,90.0,A);
	    }else
	    if(i < amount_of_nodes-1)
	    {
	        GetNodePos(node_id_array[i+1],Ax,Ay,Az);
	    	A =floatabs(270.0 - atan2( Ax-X,Ay-Y));
	    	CreateDynamicObject(1318,X,Y,Z+0.2,0.0,90.0,A);
	    }
	    else
	    {
	        CreateDynamicObject(1318,X,Y,Z+0.2,0.0,0.0,0.0);
	    }
		//printf("Point(%d)=NodeID(%d)",i,node_id_array[i]);
	}
	//Create();
	SendClientMessage(routeid,-1,"Happy route!");
	return 1;
}
/*{
	printf("Calculated %d %d",routeid,amount_of_nodes);
	//if(information[0] != 0) return 0;

	if(routeid >= 100 && amount_of_nodes > 1)
	{
		counter[routeid-100]--;
	    DestroyRoutes(routeid-100);
		new Float:lastX,Float:lastY,Float:lastZ;

		GetNodePos(node_id_array[0],lastX,lastY,lastZ);
		//GetPlayerPos(routeid-100,lastX,lastY,lastZ);

		new _max;

		if(amount_of_nodes < 50) _max = amount_of_nodes;
		else _max = 50;

		for(new i=0; i < _max; i++)
		{
		    new Float:X,Float:Y,Float:Z;
		    GetNodePos(node_id_array[i],X,Y,Z);
		    CreateMapRoute(routeid-100,lastX,lastY,X,Y,0xFF000055);
		    lastX=X;
		    lastY=Y;
			//SetPlayerMapIcon(0,i,X,Y,Z,23,0,MAPICON_LOCAL);
		}
	//print("Calculated.");
	}
	return 1;
}

public OnPlayerClosestNodeIDChange(playerid,old_NodeID,new_NodeID)
{
//	print("OnNodeChange");
	//new fromnode = NearestPlayerNode(playerid);
	if(counter[playerid] <= 30)
	{
		if(old_NodeID > 0)
		{
		    counter[playerid]++;
			CalculatePath(new_NodeID,gps_destination[playerid],playerid+100);
		}
	}
}

CreateMapRoute(playerid, Float:X1,Float:Y1,Float:X2,Float:Y2,color)
{
	new Float:Dis = 20.0; // Distance, in which distances "dots" should be created.

	new Float:TotalDis = GDBP(X1,Y1,0.0,X2,Y2,0.0); // The total distance between the two Points.

	new Points=floatround(TotalDis/Dis); // The number of dots whcih should be created, relying on the total difference / Dots distance. Very easy

	for(new i=1; i <= Points; i++)
	{
	    new Float:x,Float:y;
	    if(i != 0) // The first dot can't be calculated, because dividing through zero is not valid (see "/ Points*i" below). Secondly, directly setting x and y is faster :P
		{
			x = X1 + (((X2 - X1) / Points)*i);  // Setting off X's position by percentage.
			y = Y1 + (((Y2 - Y1) / Points)*i);  // Setting off Y's position by percentage.
		}
		else
		{
		    x=X1;
		    y=Y1;
		}

		new slot=0;

		while(slot <= MAX_DOTS)
		{
		    if(slot == MAX_DOTS)
		    {
		        slot = -1;
		        break;
		    }

		    if(Routes[playerid][slot] == -1)
		    {
		        break;
		    }
		    slot++;
		}
		if(slot == -1) return 0;
	    new zone = GangZoneCreate(x-(Dis/2),y-(Dis/2),x+(Dis/2),y+(Dis/2)); // GangZone from x/y to x/y [+5]
	    GangZoneShowForPlayer(playerid,zone,color); // -----------------------------------CHANGE!!!!!!!
	    Routes[playerid][slot]=zone;
	}

	//printf("LINECR... Distance: %f | Points: %d" , TotalDis, Points);
	return 1;
}

DestroyRoutes(playerid)
{
	for(new x; x < MAX_DOTS; x++)
	{
		if(Routes[playerid][x] != -1)
		{
		    GangZoneDestroy(Routes[playerid][x]);
		    Routes[playerid][x] = -1;
		}
	}
}*/
// RELEASED(keys)
#define RELEASED(%0) \
	(((newkeys & (%0)) != (%0)) && ((oldkeys & (%0)) == (%0)))
// PRESSED(keys)
#define PRESSED(%0) \
	(((newkeys & (%0)) == (%0)) && ((oldkeys & (%0)) != (%0)))
// HOLDING(keys)
#define HOLDING(%0) \
	((newkeys & (%0)) == (%0))

new mode = 0;
new ctr = 0;

public OnPlayerCommandText(playerid,cmdtext[])
{
	#if defined NOOOOO
	if(strcmp(cmdtext,"/gpsdoff",true) == 0)
	{
	    SetPlayerVirtualWorld(playerid,1);
	    return 1;
	}
	if(strcmp(cmdtext,"/gpsdon",true) == 0)
	{
	    SetPlayerVirtualWorld(playerid,0);
	    SetTimer("Create",50,0);
	    return 1;
	}
	if(strcmp(cmdtext,"/gotonode",true,9) == 0)
	{
	    if(strlen(cmdtext) <= 10) return SendClientMessage(playerid,-1,"Invalid NodeID.");
	    new Float:X,Float:Y,Float:Z;
	    if(GetNodePos(strval(cmdtext[10]),X,Y,Z) != 1) return SendClientMessage(playerid,-1,"Invalid NodeID.");
	    if(X == 0.0 && Y == 0.0 && Z == 0.0)  return SendClientMessage(playerid,-1,"Invalid NodeID.");
	    
	    SetPlayerPos(playerid,X,Y,Z+1);
	    return 1;
	}
	#endif
	
	return 0;
}

stock GameTextFormatForPlayer(playerid, const msg[], timeg, style, {Float,_}:...)
{
	new len = strlen(msg),d=0,posArg = 4;
 	new dest[512];
	for(new i=0;i<len;i++)
	{
	    if(msg[i] == '%')
	    {
	        switch (msg[i+1])
	        {
	            case 's':
				{
	   				new pos,arg,tmppos;
	   				new str[128];
	       			while(getarg(posArg,pos)!='\0')
	   				{
        				arg=getarg(posArg,pos++);
						str[tmppos]=arg;
      					tmppos++;
	       			}
				    strins(dest,str,d,strlen(str));
				    d+=strlen(str);
					posArg++;
					i++;
				}
	            case 'i', 'd':
				{
				    new str[128];
				    format(str,sizeof(str),"%d",getarg(posArg));
				    strins(dest,str,d,strlen(str));
				    d+=strlen(str);
					posArg++;
					i++;
				}
	            case 'f':
				{
				    new str[128];
				    format(str,sizeof(str),"%f",getarg(posArg));
				    strins(dest,str,d,strlen(str));
				    d+=strlen(str);
					posArg++;
					i++;
				}
				case '.':
				{
				    new len2 = msg[i+2];
				    if(len2 == 0)
					{
						dest[d] = msg[i];
						d++;
					}
					else
					{
					    new str[32],formatting[5];
					    formatting[0] = '%';
					    formatting[1] = '.';
					    formatting[2] = len2;
					    formatting[3] = 'f';
					    format(str,sizeof(str),formatting,getarg(posArg));
					    strins(dest,str,d,len2);
					    d+=len;
						posArg++;
						i+= 2;
					}
				}
				default:
				{
					dest[d] = msg[i];
					d++;
				}
			}
		}
		else
		{
			dest[d] = msg[i];
			d++;
		}
	}
	return GameTextForPlayer(playerid,dest,timeg,style);
}
stock SendClientFormatMessage(playerid,color,const msg[],{Float,_}:...)
{
	new len = strlen(msg),d=0,posArg = 3;
 	new dest[512];
	for(new i=0;i<len;i++)
	{
	    if(msg[i] == '%')
	    {
	        switch (msg[i+1])
	        {
	            case 's':
				{
	   				new pos,arg,tmppos;
	   				new str[128];
	       			while(getarg(posArg,pos)!='\0')
	   				{
        				arg=getarg(posArg,pos++);
						str[tmppos]=arg;
      					tmppos++;
	       			}
				    strins(dest,str,d,strlen(str));
				    d+=strlen(str);
					posArg++;
					i++;
				}
	            case 'i', 'd':
				{
				    new str[128];
				    format(str,sizeof(str),"%d",getarg(posArg));
				    strins(dest,str,d,strlen(str));
				    d+=strlen(str);
					posArg++;
					i++;
				}
	            case 'f':
				{
				    new str[128];
				    format(str,sizeof(str),"%f",getarg(posArg));
				    strins(dest,str,d,strlen(str));
				    d+=strlen(str);
					posArg++;
					i++;
				}
				case '.':
				{
				    new len2 = msg[i+2];
				    if(len2 == 0)
					{
						dest[d] = msg[i];
						d++;
					}
					else
					{
					    new str[32],formatting[5];
					    formatting[0] = '%';
					    formatting[1] = '.';
					    formatting[2] = len2;
					    formatting[3] = 'f';
					    format(str,sizeof(str),formatting,getarg(posArg));
					    strins(dest,str,d,len2);
					    d+=len;
						posArg++;
						i+= 2;
					}
				}
				default:
				{
					dest[d] = msg[i];
					d++;
				}
			}
		}
		else
		{
			dest[d] = msg[i];
			d++;
		}
	}
	return SendClientMessage(playerid,color,dest);
}
